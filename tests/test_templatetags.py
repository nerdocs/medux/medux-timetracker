from datetime import timedelta


from medux.common.templatetags.medux import as_hours


def test_hours_filter():
    assert as_hours(timedelta(0)) == 0

    assert as_hours(timedelta(hours=2)) == 2.0
    assert as_hours(timedelta(hours=2, minutes=15)) == 2.25

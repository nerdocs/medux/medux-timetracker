

from medux.tests.settings import *

# force .env providing a DB backend.
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "timetracker.db",
        # "USER": env("DATABASE_USER"),
        # "PASSWORD": env("DATABASE_PASS"),
    }
}

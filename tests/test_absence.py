from datetime import datetime, date

import pytest

from medux.plugins.timetracker.constants import ABSENCE_TYPE_HOLIDAY
from medux.plugins.timetracker.models import (
    TimeEntry,
    Absence,
    AbsenceStatus,
    AbsenceType,
)

from django.utils import timezone


@pytest.mark.django_db
def test_deleting_absence_deletes_all_timeentries(employee_a1):
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(2020, 10, 1, 8, 15, tzinfo=timezone.utc),
        end=datetime(2020, 10, 1, 8, 20, tzinfo=timezone.utc),
    )
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(2020, 10, 1, 8, 30, tzinfo=timezone.utc),
    )
    # when this absence is created, no TEs above must be deleted.
    absence = Absence.objects.create(
        employee=employee_a1,
        day=date(2020, 10, 1),
        absence_type=AbsenceType.get_by_uuid(employee_a1.tenant, ABSENCE_TYPE_HOLIDAY),
    )
    assert len(TimeEntry.objects.all()) == 2

    # after saving the Absence with status=APPROVED, all TEs of that day/employee must be gone
    absence.status = AbsenceStatus.APPROVED
    absence.save()
    assert len(TimeEntry.objects.all()) == 0

import pytest

from medux.plugins.timetracker.models import AbsenceType


@pytest.mark.django_db
def test_tenant_absence_types(tenant_a):
    # create a tenant-local copy of the holiday AbsenceType
    absence_types = tenant_a.absence_types.all()
    assert len(absence_types) == 5

    AbsenceType.objects.create(
        label="Something else",
        # uuid = new one
        tenant=tenant_a,
    )
    assert tenant_a.absence_types.count() == 6

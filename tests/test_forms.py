import zoneinfo
from datetime import datetime, time

import pytest
from django.utils import timezone
from django.utils.formats import time_format
from month import Month

from medux.plugins.timetracker.forms import (
    TimeEntryForm,
    LogoutWithTimeTrackingForm,
    MonthlyTimeBillingForm,
)
from medux.plugins.timetracker.models import (
    TimeEntry,
    MonthlyTimeBilling,
)


@pytest.mark.django_db
def test_timeentry_form_create(employee_a1):
    # provide a date, but no start time
    form = TimeEntryForm(data={"date": "2023-01-21"}, employee=employee_a1)
    assert form.is_bound
    assert form.is_valid() is False
    assert form.has_error("start_time", "required")


@pytest.mark.django_db
def test_timeentry_form_create_with_start_time(employee_a1):
    form = TimeEntryForm(
        data={"date": "2023-01-21", "start_time": "08:00"}, employee=employee_a1
    )
    assert form.is_valid()


@pytest.mark.django_db
def test_timeentry_form_create_with_empty_start_time(employee_a1):
    form = TimeEntryForm(
        data={"date": "2023-01-21", "start_time": ""}, employee=employee_a1
    )
    assert form.has_error("start_time", "required")


@pytest.mark.django_db
def test_timeentry_form_create_with_wrong_start_time(employee_a1):
    form = TimeEntryForm(
        data={"date": "2023-01-21", "start_time": "25:00"}, employee=employee_a1
    )
    assert form.has_error("start_time", "invalid")


@pytest.mark.django_db
def test_timeentry_form_create_with_existing_open_timeentry(employee_a1):
    # create an open TimeEntry.
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(2020, 10, 10, 8, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )
    # try to add another open TimeEntry 2 days later using the TimeEntryForm
    form = TimeEntryForm(
        data={"date": "10.10.2020", "start_time": "8:00"}, employee=employee_a1
    )
    assert form.non_field_errors()


@pytest.mark.django_db
def test_timeentry_form_invalid_same_start_end_time(employee_a1):
    form = TimeEntryForm(
        data={
            "date": timezone.now().date(),
            "start_time": "09:00",
            "end_time": "09:00",
        },
        employee=employee_a1,
    )
    assert form.has_error("end_time", "end-not-same-start")

    # same again, assert that end time gets rounded up.
    form = TimeEntryForm(
        data={
            "date": timezone.now().date(),
            "start_time": "09:00",
            "end_time": "08:59",
        },
        employee=employee_a1,
    )
    assert form.has_error("end_time", "end-not-same-start")


@pytest.mark.django_db
def test_timeentry_form_invalid_start_before_end_time(employee_a1):
    form = TimeEntryForm(
        data={
            "date": "23.12.2023",
            "start_time": "09:00",
            "end_time": "08:45",
        },
        employee=employee_a1,
    )
    assert not form.is_valid()
    assert form.has_error("end_time", "end-not-before-start")


@pytest.mark.django_db
def test_timeentry_form_update(employee_a1):
    instance = TimeEntry(
        employee=employee_a1,
        start=datetime(2020, 1, 1, 8, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
        end=datetime(2020, 1, 1, 10, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )
    data = {
        "date": timezone.localdate(instance.start),
        "start_time": timezone.localtime(instance.start).time(),
        "end_time": "10:15",
    }
    form = TimeEntryForm(employee=employee_a1, instance=instance, data=data)
    assert form.is_valid()


@pytest.mark.django_db
def test_logout_with_timeentry_form(employee_a1):
    initial = {"date": "2023-02-12"}
    data = {"start_time": "09:00"}

    form = LogoutWithTimeTrackingForm(employee=employee_a1, initial=initial, data=data)
    assert form.is_valid() is False

    data["end_time"] = "09:45"
    form = LogoutWithTimeTrackingForm(employee=employee_a1, initial=initial, data=data)
    assert form.is_valid()


@pytest.mark.django_db
def test_absence_form():
    ...


@pytest.mark.django_db
def test_monthly_time_billing_form():
    ...


@pytest.mark.django_db
def test_timeentry_form_invalid_monthlybilling_already_submitted_for_that_month(
    employee_a1,
):
    month = Month(2020, 3)
    billing = MonthlyTimeBilling(employee=employee_a1, month=month)
    billing.recalculate()
    billing.save()

    form = TimeEntryForm(
        employee=employee_a1,
        data={
            "date": month.first_day(),
            "start_time": "09:00",
            "end_time": "17:00",
        },
    )
    assert not form.is_valid()
    assert form.non_field_errors()

    form = TimeEntryForm(
        employee=employee_a1,
        data={
            "date": (month + 1).first_day(),
            "start_time": "00:00",
            "end_time": "04:00",
        },
    )
    assert form.is_valid()


@pytest.mark.django_db
def test_monthlybilling_submitted_still_open_timeentry(employee_a1):
    # create a timeentry at Jan 1st 2021, Europe/Vienna, which is before midnight in UTC
    month = Month(2021, 1)
    entry = TimeEntry.objects.create(
        employee=employee_a1,
        start=timezone.make_aware(datetime.combine(month.first_day(), time(0, 30))),
    )

    instance = MonthlyTimeBilling(employee=employee_a1, month=month)
    instance.recalculate()

    form = MonthlyTimeBillingForm(instance=instance, data={})
    assert not form.is_valid()
    # TODO look more specific which error was thrown:
    assert form.non_field_errors()

    # now put that entry into february
    entry.start = timezone.make_aware(datetime(2021, 2, 13, 14, 30))
    entry.save()
    instance.recalculate()
    form = MonthlyTimeBillingForm(instance=instance, data={})
    assert form.is_valid()


@pytest.mark.django_db
def test_timeentry_form_save(employee_a1):
    start = datetime(2022, 1, 1, 9, 0, 0, tzinfo=timezone.utc)
    end = datetime(2022, 1, 1, 17, 0, 0, tzinfo=timezone.utc)
    form = TimeEntryForm(
        employee=employee_a1,
        initial={"date": "2022-01-01"},
        data={
            "date": timezone.localdate(start),
            "start_time": time_format(timezone.localtime(start)),
            "end_time": time_format(timezone.localtime(end)),
            "comment": "Test comment",
        },
    )
    time_entry = form.save()
    assert time_entry.employee == employee_a1
    assert time_entry.start == start
    assert time_entry.end == end
    assert time_entry.comment == "Test comment"

from datetime import datetime, date

import pytest
from django.core.management import call_command
from django.utils import timezone

from medux.employees.tests.conftest import (  # noqa
    tenant_a,
    classification_foo,
    application_med_assistant,
)
from medux.employees.models import WorkingContract
from medux.plugins.timetracker.constants import (
    ABSENCE_TYPE_STANDARD,
)
from medux.plugins.timetracker.models import (
    TimetrackerEmployee,
    WorkSchedule,
    HolidayPolicy,
    AbsenceType,
)


@pytest.fixture(scope="session")
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command("initialize")
        call_command("loadpreferences")


@pytest.fixture
def employee_a1(tenant_a) -> TimetrackerEmployee:
    return TimetrackerEmployee.objects.create_user(
        username="employee_a1", password="password", tenant=tenant_a
    )


@pytest.fixture
def employee_b1(tenant_b):
    return TimetrackerEmployee.objects.create_user(
        username="employee_b1", password="password", tenant=tenant_b
    )


@pytest.fixture
def workingcontract_2019(employee_a1, classification_foo, application_med_assistant):
    """returns a WorkingContract object, without saving it."""
    return WorkingContract.objects.create(
        employee=employee_a1,
        tenant=employee_a1.tenant,
        start_date=date(2019, 1, 1),
        end_date=date(2019, 12, 31),
        employment_date=date(2019, 1, 1),
        classification=classification_foo,
        intended_application=application_med_assistant,
        initial_salary=0,
    )


@pytest.fixture
def workingcontract_2019ff(employee_a1, classification_foo, application_med_assistant):
    """returns a WorkingContract object, without saving it."""
    return WorkingContract.objects.create(
        employee=employee_a1,
        tenant=employee_a1.tenant,
        start_date=date(2019, 1, 1),
        employment_date=date(2019, 1, 1),
        classification=classification_foo,
        intended_application=application_med_assistant,
        initial_salary=0,
    )


@pytest.fixture
def absence_type_std_a(tenant_a):
    return AbsenceType.objects.get(uuid=ABSENCE_TYPE_STANDARD, tenant=tenant_a)


@pytest.fixture
def holidaypolicy30_a(tenant_a, absence_type_std_a):
    return HolidayPolicy.objects.create(
        tenant=tenant_a,
        name="test policy",
        days_per_year=30,
        absence_type=absence_type_std_a,
    )


@pytest.fixture
def workschedule_march_2019(employee_a1, workingcontract_2019):
    return WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019,
        start_date=datetime(year=2019, month=3, day=1, tzinfo=timezone.utc),
        end_date=datetime(year=2019, month=3, day=31, tzinfo=timezone.utc),
    )


@pytest.fixture
def workschedule_2020_jan01_jan10(employee_a1, workingcontract_2019):
    """create WorkSchedule for January 2020, first 10 days, in current timezone"""
    return WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019,
        start_date=date(2020, 1, 1),
        end_date=date(2020, 1, 10),
    )


@pytest.fixture
def workschedule_2020_jan10_jan31(employee_a1, workingcontract_2019):
    """create WorkSchedule for January 2000, second 21 days"""
    return WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019,
        start_date=date(2020, 1, 11),
        end_date=date(2020, 1, 31),
    )

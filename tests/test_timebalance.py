import pytest
from django.utils import timezone

from medux.plugins.timetracker.models import TimeBalanceEntry


@pytest.mark.django_db
def test_time_balance_entry_creation(employee_a1):
    initial_balance = timezone.timedelta(hours=8)
    entry = TimeBalanceEntry.objects.create(
        employee=employee_a1,
        time_delta=initial_balance,
    )
    assert entry.balance == initial_balance

    time_delta = timezone.timedelta(hours=2)
    entry = TimeBalanceEntry.objects.create(
        employee=employee_a1,
        time_delta=time_delta,
    )
    assert entry.balance == initial_balance + time_delta

    negative_delta = timezone.timedelta(hours=-3)
    entry = TimeBalanceEntry.objects.create(
        employee=employee_a1,
        time_delta=negative_delta,
    )
    assert entry.balance == initial_balance + time_delta + negative_delta

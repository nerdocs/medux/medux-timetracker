from datetime import time, timedelta

import pytest

from medux.employees.models import WorkingTimeRange


@pytest.mark.django_db
def test_duration(employee_a1):
    wtr_8_12 = WorkingTimeRange(
        start_time=time(hour=8, minute=0),
        end_time=time(hour=12, minute=0),
        weekday=0,
    )
    assert wtr_8_12.duration() == timedelta(hours=4)
    assert wtr_8_12.duration_h() == 4

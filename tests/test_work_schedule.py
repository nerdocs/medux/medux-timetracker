from datetime import datetime, date, time

import pytest
from django.utils import timezone
from django.utils.timezone import get_current_timezone

from medux.employees.models import WorkingTimeRange
from medux.plugins.timetracker.models import (
    TimeEntry,
    WorkSchedule,
    TimetrackerEmployee,
)


@pytest.mark.django_db
def test_workschedule_creation_creates_holidayentitlements(
    employee_a1: TimetrackerEmployee, workingcontract_2019ff, holidaypolicy30_a
):
    employee_a1.apply_holiday_policy(holidaypolicy30_a, date(2019, 1, 1))
    ws = WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019ff,
        start_date=date(2019, 1, 1),
        end_date=date(2019, 1, 31),
    )
    WorkingTimeRange.objects.create(
        work_schedule=ws, start_time=time(8, 0), end_time=time(12, 00), weekday=1
    )
    assert employee_a1.holiday_entitlements.count() == 3

    ws = WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019ff,
        start_date=date(2019, 2, 1),
        end_date=date(2019, 10, 15),
    )
    WorkingTimeRange.objects.create(
        work_schedule=ws, start_time=time(8, 0), end_time=time(12, 00), weekday=2
    )
    assert employee_a1.holiday_entitlements.count() == 6

    # create WS without end
    ws = WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019ff,
        start_date=date(2019, 10, 16),
    )
    WorkingTimeRange.objects.create(
        work_schedule=ws, start_time=time(8, 0), end_time=time(12, 00), weekday=3
    )
    # here, only HEs until  next leave year's end should be created.
    assert employee_a1.holiday_entitlements.count() == 9


@pytest.mark.django_db
def test_correct_auto_assignment_to_work_schedules(
    employee_a1, workschedule_2020_jan01_jan10, workschedule_2020_jan10_jan31
):
    # create 31 entries in one go, daily 8-16:00, don't assign WorkSchedules to them.
    for day in range(1, 31):
        # we have to use .create() to call the save() method, so WorkSchedules get assigned
        TimeEntry.objects.create(
            employee=employee_a1,
            start=datetime(2020, 1, day, 8, 0, tzinfo=get_current_timezone()),
            end=datetime(2020, 1, day, 16, 0, tzinfo=get_current_timezone()),
        )
    assert (
        len(
            TimeEntry.objects.filter(
                employee=employee_a1, work_schedule=workschedule_2020_jan01_jan10
            )
        )
        == 10
    )
    assert (
        len(
            TimeEntry.objects.filter(
                employee=employee_a1, work_schedule=workschedule_2020_jan10_jan31
            )
        )
        == 20
    )


@pytest.mark.django_db
def test_changing_work_schedule_dates_should_update_time_entries(
    employee_a1, workschedule_2020_jan01_jan10, workschedule_2020_jan10_jan31
):
    """Make sure that if a WorkSchedule changes start/end date, corresponding TimeEntries are shifted to
    the new Schedule."""

    for day in range(1, 31):
        # we have to use .create() to call the save() method, so WorkSchedules get assigned
        TimeEntry.objects.create(
            employee=employee_a1,
            start=timezone.make_aware(datetime(2020, 1, day, 8, 0)),
            end=timezone.make_aware(datetime(2020, 1, day, 16, 0)),
        )
    # shorten schedule1 time period (10->8), and augment schedule2 (11->9)
    workschedule_2020_jan01_jan10.end_date = date(2020, 1, 8)
    workschedule_2020_jan01_jan10.save()
    workschedule_2020_jan10_jan31.start_date = date(2020, 1, 9)
    workschedule_2020_jan10_jan31.save()
    assert (
        TimeEntry.objects.get(
            employee=employee_a1,
            start=timezone.make_aware(datetime(2020, 1, 9, 8, 0)),
        ).work_schedule
        == workschedule_2020_jan10_jan31
    )

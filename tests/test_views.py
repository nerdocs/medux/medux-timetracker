import zoneinfo
import calendar
from datetime import datetime, time

import pytest
from django.test import Client
from django.urls import reverse
from django.utils import timezone

from medux.employees.models import WorkingTimeRange
from medux.plugins.timetracker.models import TimeEntry


@pytest.mark.django_db
def test_correct_login(client: Client, employee_a1):
    response = client.post(
        reverse("login"), data={"username": "employee_a1", "password": "password"}
    )
    # 302 is a redirect; 200 would mean the login page was loaded
    assert response.status_code == 302
    assert "_auth_user_id" in client.session


@pytest.mark.django_db
def test_login_with_come__timeentry_created(client: Client, employee_a1):
    """create a timeentry using login form plugin"""
    response = client.post(
        reverse("login"),
        data={
            "username": "employee_a1",
            "password": "password",
            "come_go": "come",
            "log_time": "00:15",  # FIXME: this will fail when test runs at <= 00:15
        },
    )
    assert response.status_code == 302
    entries = TimeEntry.objects.filter(employee=employee_a1)
    assert entries.exists() and len(entries) == 1
    entry = entries.first()
    assert entry.start == timezone.make_aware(
        datetime.now().replace(hour=0, minute=15, second=0, microsecond=0)
    )
    assert entry.end is None


@pytest.mark.django_db
def test_login_with_go__timeentry_updated(client: Client, employee_a1):
    """update a started timeentry using login form plugin"""

    # create a timeentry with start today 05:00
    TimeEntry.objects.create(
        employee=employee_a1,
        start=timezone.make_aware(
            datetime.now().replace(hour=5, minute=0, second=0, microsecond=0)
        ),
    )
    response = client.post(
        reverse("login"),
        data={
            "username": "employee_a1",
            "password": "password",
            "come_go": "go",
            "log_time": "06:00",
        },
    )
    assert response.status_code == 302
    entries = TimeEntry.objects.filter(employee=employee_a1)
    assert entries.exists() and len(entries) == 1
    entry = entries.first()
    assert entry.start == timezone.make_aware(
        datetime.now().replace(hour=5, minute=0, second=0, microsecond=0)
    )
    assert entry.end == timezone.make_aware(
        datetime.now().replace(hour=6, minute=0, second=0, microsecond=0)
    )


@pytest.mark.django_db
def test_login_with_2_open_timeentries(client: Client, employee_a1):
    """update a started timeentry using login form plugin"""

    # an old christmas present...
    TimeEntry.objects.create(
        employee=employee_a1,
        start=timezone.make_aware(
            datetime(2020, 12, 24, hour=19, minute=00, second=0, microsecond=0)
        ),
    )
    # create a timeentry with start today 05:00
    TimeEntry.objects.create(
        employee=employee_a1,
        start=timezone.make_aware(
            datetime.now().replace(hour=5, minute=0, second=0, microsecond=0)
        ),
    )
    response = client.post(
        reverse("login"),
        data={
            "username": "employee_a1",
            "password": "password",
            "come_go": "go",
            "log_time": "06:00",
        },
    )
    assert response.status_code == 200
    assert "error_1_id_log_time" in response.rendered_content


@pytest.mark.django_db
def test_timeentry_month_list_view(client: Client, employee_a1):
    # Create a timeentry on 2019-03-05 at 0:30 in TZ Vienna (+01:00),
    # which means UTC 2019-03-04 23:30
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(
            2019, 3, 5, 0, 30, tzinfo=zoneinfo.ZoneInfo(key="Europe/Vienna")
        ),
        end=datetime(2019, 3, 5, 10, 30, tzinfo=zoneinfo.ZoneInfo(key="Europe/Vienna")),
    )
    client.force_login(employee_a1)
    client.get(reverse("timetracker:timeentry:month-list"))


@pytest.mark.django_db
def test_timeentry_month_list_view_with_absences(
    client: Client, employee_a1, workschedule_2020_jan01_jan10
):

    # employee_a1.groups.add(Group.objects.get_or_create(name="Users")[0])
    # employee_a1.refresh_from_db()
    client.force_login(employee_a1)
    # Jan 3, 2020 was a Friday, so work on Fridays
    WorkingTimeRange.objects.create(
        work_schedule=workschedule_2020_jan01_jan10,
        start_time=time(10, 0),
        end_time=time(12, 0),
        weekday=calendar.FRIDAY,
    )
    response = client.get(reverse("timetracker:timeentry:month-list", args=(2020, 1)))
    assert response.status_code == 200


# def test_add_absence(client):
#     client.force_login(employee_a1)
#     htmx_headers = {"HTTP_HX-Request": "true"}
#     response = client.get(
#         reverse(
#             "timetracker:absence:add",
#             kwargs={"absence_type": ABSENCE_TYPE_HOLIDAY},
#         ),
#         **htmx_headers,
#     )
#     # FIXME: I really don't know why this test sometimes passes, and most of the times not.
#     assert response.status_code == 200

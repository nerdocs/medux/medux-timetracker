import pytest
from django.core.exceptions import ValidationError

from medux.plugins.timetracker.constants import (
    ABSENCE_TYPE_ILLNESS,
    ABSENCE_TYPE_PRESETS,
)
from medux.plugins.timetracker.models import AbsenceType


@pytest.mark.django_db
def test_autocreate_from_tenant(tenant_a):
    at = tenant_a.absence_types.all()
    assert len(at) == len(ABSENCE_TYPE_PRESETS)


# test creating of a preset AbsenceType with the .create() method
@pytest.mark.django_db
def test_create_absence_type_from_preset(tenant_a):
    # delete already created presets
    AbsenceType.objects.all().delete()

    absence_type = AbsenceType.objects.create(
        uuid=ABSENCE_TYPE_ILLNESS, tenant=tenant_a
    )
    assert absence_type.label == "Illness"

    with pytest.raises(ValidationError):
        # creating a preset AbsenceType without a tenant is forbidden.
        AbsenceType.objects.create(uuid=ABSENCE_TYPE_ILLNESS)

import zoneinfo

import pytest
from datetime import timedelta, datetime, date
from django.core.exceptions import ValidationError
from django.utils import timezone

from medux.employees.models import WorkSchedule
from medux.plugins.timetracker.models import TimeEntry
from medux.employees.tests.conftest import tenant_a  # noqa

SCHEDULE_START = date(2020, 1, 1)
SCHEDULE_END = date(2020, 12, 31)


@pytest.fixture()
def work_schedule_2020(employee_a1, workingcontract_2019):
    return WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_2019,
        start_date=SCHEDULE_START,
        end_date=SCHEDULE_END,
    )


@pytest.fixture
def time_entry(employee_a1, work_schedule_2020):
    """Create a TimeEntry with a start time and an end time"""
    return TimeEntry.objects.create(
        employee=employee_a1,
        work_schedule=work_schedule_2020,
        start=datetime(2019, 6, 21, 15, 0, tzinfo=timezone.utc),
        end=datetime(2019, 6, 21, 17, 0, tzinfo=timezone.utc),
        comment="A comment",
    )


@pytest.mark.django_db
def test_duration(time_entry):
    assert time_entry.duration() == timedelta(hours=2)


@pytest.mark.django_db
def test_duration_across_utc_day_boundaries(employee_a1):
    """create a TimeEntry shortly after 0:00 in a timezone, which is <0:00 in UTC
    and with an end  far enough within the day.
    start / end are both at one date in the local timezone, but at different dates in UTC
    """
    entry = TimeEntry(
        employee=employee_a1,
        start=datetime(2019, 6, 21, 0, 30, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
        end=datetime(2019, 6, 21, 4, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )
    assert entry.duration() == timedelta(hours=3, minutes=30)
    assert entry.duration_h() == 3.5

    entry = TimeEntry(
        employee=employee_a1,
        start=datetime(2023, 3, 24, 1, 15, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
        end=datetime(2023, 3, 24, 4, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )


@pytest.mark.django_db
def test_duration_h(time_entry):
    assert time_entry.duration_h() == 2.0


@pytest.mark.django_db
def test_is_weekend(time_entry):
    assert not time_entry.is_weekend()


@pytest.mark.django_db
def test_save(time_entry):
    time_entry.save()
    assert time_entry.work_schedule is not None


@pytest.mark.django_db
def test_save_without_work_schedule(employee_a1, work_schedule_2020):
    timestamp = datetime(2020, 3, 21, 19, 30, tzinfo=timezone.utc)
    time_entry = TimeEntry.objects.create(
        employee=employee_a1,
        start=timestamp - timezone.timedelta(hours=2),
        end=timestamp,
        comment="A comment",
    )
    time_entry.save()
    assert time_entry.work_schedule is not None


@pytest.mark.django_db
def test_cannot_save_end_before_start(employee_a1, work_schedule_2020):
    with pytest.raises(ValidationError):
        TimeEntry.objects.create(
            employee=employee_a1,
            work_schedule=work_schedule_2020,
            start=timezone.now(),
            end=timezone.now() - timezone.timedelta(hours=2),
        ).full_clean()


@pytest.mark.django_db
def test_daily_time_entry_sum(employee_a1):
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(2020, 10, 11, 8, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
        end=datetime(2020, 10, 11, 12, 00, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(2020, 10, 11, 14, 0, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
        end=datetime(2020, 10, 11, 16, 00, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )
    assert (
        employee_a1.daily_time_entry_sum(day=date(2020, 10, 11)).total_seconds()
        == 6 * 60 * 60
    )


@pytest.mark.django_db
def test_daily_time_entry_sum_across_utc_day_boundaries(employee_a1):
    # test across utc boundaries
    TimeEntry.objects.create(
        employee=employee_a1,
        start=datetime(2020, 10, 11, 0, 15, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
        end=datetime(2020, 10, 11, 3, 15, tzinfo=zoneinfo.ZoneInfo("Europe/Vienna")),
    )
    assert (
        employee_a1.daily_time_entry_sum(day=date(2020, 10, 11)).total_seconds()
        == 3 * 60 * 60
    )

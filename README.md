# MedUX Timetracker plugin

## General

MedUX is an Open Source Electronic Record, in an early (currently Pre-Alpha) state.

This is a plugin which provides timetracker, holiday planning, and work schedule functionality.

It is in pre-alpha state, so don't use it in production, unless you are very brave, like I am.

## Install

As usual in MedUX:

```bash
pip install medux-timetracker
./manage.py migrate
./manage.py initialize # to update missing groups/permissions
./manage.py runserver
```

## License

This plugin is licensed under the [GNU Affero General Public License v3 or later (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.txt)

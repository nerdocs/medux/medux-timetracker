all: localegen localecompile
production: localecompile
LANGUAGES:=`find src/medux/plugins/timetracker/locale/ -mindepth 1 -maxdepth 1 -type d -printf "--locale %f "`

localecompile:
	django-admin compilemessages

localegen:
	django-admin makemessages --ignore "static/*" --ignore "medux/plugins/timetracker/static/*" --ignore "build/*" $(LANGUAGES)
	django-admin makemessages -d djangojs --ignore "static/*" --ignore "medux/plugins/timetracker/static/*" --ignore "build/*" $(LANGUAGES)

test:
	pytest

from uuid import UUID

from medux.common.constants import UserColors
from django.utils.translation import gettext_lazy as _

# define some presets that are available in all appliances.
ABSENCE_TYPE_ILLNESS = UUID("3e8214b8-4fb1-448b-ad53-17f74b2e096a")
ABSENCE_TYPE_HOLIDAY = UUID("78fe82be-2a36-48b8-a137-3a997267e4f6")
ABSENCE_TYPE_SPECIAL = UUID("24b1ad0e-fedc-4196-a12f-9ce2b293675d")
ABSENCE_TYPE_TRAINING = UUID("567c0e42-a3b9-45be-9204-45e6e66e477c")
ABSENCE_TYPE_CTO = UUID("13d05b0f-f121-4eae-ad1a-96d237c83e1b")

ABSENCE_TYPE_PRESETS = {
    ABSENCE_TYPE_ILLNESS: {
        "label": _("Illness"),
        "color": UserColors.DARK,
        "uuid": ABSENCE_TYPE_ILLNESS,
    },
    ABSENCE_TYPE_HOLIDAY: {
        "label": _("Holiday"),
        "color": UserColors.AZURE,
        "uuid": ABSENCE_TYPE_HOLIDAY,
    },
    ABSENCE_TYPE_SPECIAL: {
        "label": _("Special"),
        "color": UserColors.INDIGO,
        "uuid": ABSENCE_TYPE_SPECIAL,
    },
    ABSENCE_TYPE_TRAINING: {
        "label": _("Training"),
        "color": UserColors.AZURE,
        "uuid": ABSENCE_TYPE_TRAINING,
    },
    ABSENCE_TYPE_CTO: {
        "label": _("CTO"),
        "color": UserColors.DARK,
        "counts_as_negative_working_time": True,
        "uuid": ABSENCE_TYPE_CTO,
    },
}

ABSENCE_TYPE_STANDARD = ABSENCE_TYPE_HOLIDAY

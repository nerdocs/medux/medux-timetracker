import decimal
from datetime import datetime, timedelta, date
import calendar

from crispy_forms.bootstrap import PrependedText, Alert, AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Column, Row, Submit
from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.translation import gettext as _
from month import Month

from medux.common.forms.widgets import DatePickerInput
from medux.common.forms.mixins import DependencyFormMixin
from medux.common.templatetags.medux import as_hours
from medux.common.tools import round_down_time, round_up_time
from medux.employees.models import WorkSchedule
from medux.plugins.timetracker.bootstrap import TimeSelector
from medux.plugins.timetracker.constants import (
    ABSENCE_TYPE_ILLNESS,
    ABSENCE_TYPE_HOLIDAY,
)
from medux.plugins.timetracker.models import (
    TimeEntry,
    Absence,
    AbsenceStatus,
    MonthlyTimeBilling,
    TimetrackerEmployee,
    HolidayPolicy,
    HolidayPolicyApplication,
    AbsenceType,
)


class TimeEntryForm(ModelForm):
    """Form for creating TimeEntries.

    Attributes:
        (as kwargs)
        employee: The employee to create a TimeEntry for.
        initial: a dict with at least "date" as a key.
            Passed times must be timezone aware.
    """

    class Meta:
        model = TimeEntry
        # `date`, `start_time`, `end_time` are no model fields.
        # The fields `start`, `end` are constructed from them in the clean() method.
        fields = ["start", "end", "comment"]

    class Media:
        js = ("timetracker/js/timeselector.js",)

    # Model fields
    start = forms.DateTimeField(required=False, widget=forms.HiddenInput())
    end = forms.DateTimeField(required=False, widget=forms.HiddenInput())

    # helper fields
    date = forms.DateField(widget=DatePickerInput)
    start_time = forms.TimeField()
    end_time = forms.TimeField(required=False)

    def __init__(self, **kwargs):
        self.employee = kwargs.pop("employee")
        super().__init__(**kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            "date",
            "start",
            TimeSelector("start_time", autofocus=True),
            "end",
            TimeSelector("end_time"),
            "comment",
        )

    def clean(self):
        # add employee to instance, so that following TimeEntry.clean() can use it.
        self.instance.employee = self.employee

        # check for valid start_time, round it down and put it into start field
        start_time = self.cleaned_data.get("start_time")
        if start_time:
            start_time = round_down_time(start_time, 15)

            start = timezone.make_aware(
                datetime.combine(
                    self.cleaned_data["date"],
                    start_time,
                )
            )
            self.cleaned_data["start"] = start

            # don't allow starts at already submitted months
            month = Month(
                timezone.localdate(start).year, timezone.localdate(start).month
            )
            if MonthlyTimeBilling.objects.filter(
                employee=self.instance.employee,
                month=month,
            ).exists():
                raise ValidationError(
                    _(
                        "%(month)s %(year)s is already submitted. No new time entries can be created for it."
                    ),
                    code="month-already-submitted",
                    params={
                        "month": _(calendar.month_name[month.month]),
                        "year": month.year,
                    },
                )

        # check for valid end time, if there, round it up
        end_time = self.cleaned_data.get("end_time")
        if end_time:
            end_time = round_up_time(end_time, 15)

            # end cannot be start time
            if end_time == start_time:
                self.add_error(
                    "end_time",
                    ValidationError(
                        _("End time cannot be same as start time."),
                        code="end-not-same-start",
                    ),
                )

            # end time must be after start time
            elif end_time < start_time:
                self.add_error(
                    "end_time",
                    ValidationError(
                        _("End time cannot be before start time."),
                        code="end-not-before-start",
                    ),
                )
            # put end time into end field
            self.cleaned_data["end"] = timezone.make_aware(
                datetime.combine(self.cleaned_data["date"], end_time)
            )
        else:
            # no end given. There may not be another open one.
            open_timeentries = TimeEntry.objects.filter(
                employee=self.employee, end__isnull=True
            )
            if open_timeentries:
                self.add_error(
                    None,
                    ValidationError(
                        _(
                            "There is already an open time entry at {date}. Please close this first."
                        ).format(
                            date=date_format(
                                timezone.localdate(open_timeentries.first().start)
                            )
                        ),
                        code="invalid",
                    ),
                )
        return super().clean()

    def clean_date(self):
        date = self.cleaned_data["date"]
        first_of_month = date.replace(day=1)
        if MonthlyTimeBilling.objects.filter(
            employee=self.employee, month=first_of_month
        ).exists():
            self.add_error(
                "date",
                ValidationError(
                    _(
                        "{month} {year} is already submitted. "
                        "It is not possible to add time entries for it."
                    ).format(month=_(calendar.month_name[date.month]), year=date.year),
                    code="invalid",
                ),
            )
        return date

    def save(self, commit=True):
        # if new TimeEntry was created, set work_schedule to default
        instance = super().save(commit=False)
        instance.work_schedule = WorkSchedule.get_matching(
            self.employee, instance.start
        )
        instance.save()
        return instance


class LogoutWithTimeTrackingForm(TimeEntryForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields["end_time"].required = True
        self.fields["date"].disabled = True


class AbsenceForm(ModelForm):
    class Meta:
        model = Absence
        fields = []

    first_day = forms.DateField(
        label=_("First day"),
        widget=DatePickerInput(end_field_name="last_day"),
    )
    last_day = forms.DateField(label=_("Last day"), widget=DatePickerInput)

    def __init__(self, request, absence_type: str, **kwargs):
        self.request = request
        self.employee = TimetrackerEmployee.objects.get(pk=request.employee.pk)
        self.absence_type = absence_type
        super().__init__(**kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Alert(
                _("WARNING: all time entries on that days will be deleted."),
                dismiss=False,
                css_class="alert-warning",
            ),
            "first_day",
            "last_day",
        )

    def save(self, commit=True):
        first_day: date = self.cleaned_data.get("first_day")
        last_day: date = self.cleaned_data.get("last_day")
        if self.absence_type in [ABSENCE_TYPE_ILLNESS, ABSENCE_TYPE_HOLIDAY]:
            status = AbsenceStatus.NEW
        else:
            raise AttributeError(f"Absence type '{self.absence_type}' not supported.")
        absences = []
        day = first_day - timedelta(days=1)
        while day < last_day:
            day += timedelta(days=1)
            collisions = Absence.objects.filter(day=day, employee=self.employee)
            if self.absence_type == ABSENCE_TYPE_ILLNESS:
                # if there is no work schedule or contract at all, don't allow sick days...
                if not self.employee.active_work_schedule(day):
                    continue
                if not self.employee.active_working_contract(day):
                    continue

                # delete all time entries on that days, regardless of being a working day or not.
                # TODO: move to admin form
                # TimeEntry.objects.filter(
                #     employee=self.employee, start__date=day
                # ).delete()

                if not self.employee.is_working_day(day):
                    # Skip creating illness days on non-working days.
                    # this also includes already approved illness days
                    continue
                # if there is a holiday at the illness request day - it can coexist, and must be
                # coped with by the admin!

            elif collisions.exists():
                # for all other types (holiday, special, training, etc.)
                # make sure that there isn't ANY other absence there.
                # any other absence type exists already for that day.
                continue

            absence = Absence(
                day=day,
                employee=self.employee,
                type=self.absence_type,
                status=status,
            )
            absences.append(absence)

        if commit:
            Absence.objects.bulk_create(absences)
            # TODO make sure all TimeEntries are deleted on that days!
        return absences


class MonthlyTimeBillingForm(forms.ModelForm):
    class Meta:
        model = MonthlyTimeBilling
        fields = [
            "payout_time_credit_hours",
        ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        (
            self.instance.time_delta.total_seconds() / 3600
            if self.instance.time_delta
            else 0.0
        )
        max_payout_hours = self._max_payout_hours()
        if max_payout_hours > 0:
            self.initial["payout_time_credit_hours"] = 0.0
            self.fields["payout_time_credit_hours"].max_value = self._max_payout_hours()
            self.fields["payout_time_credit_hours"].min_value = 0
        else:
            self.fields["payout_time_credit_hours"].disabled = True
        self.helper = FormHelper()
        self.helper.form_tag = False
        if max_payout_hours > 0:
            self.helper.layout = Layout(
                PrependedText(
                    "payout_time_credit_hours",
                    _("max. {amount} hours:").format(amount=max_payout_hours),
                )
            )

    def _max_payout_hours(self) -> decimal.Decimal:
        """Return the maximum amount of hours that can be paid out.

        This is restricted tho the current month's time delta.
        """
        return as_hours(
            # self.instance.employee.time_account_balance() +
            self.instance.time_delta
        )

    def clean_payout_time_credit_hours(self):
        """Don't let users pay out more hours than credit is available"""
        balance = as_hours(self.instance.employee.time_account_balance())
        requested_payout = self.cleaned_data["payout_time_credit_hours"]
        month_time_delta = decimal.Decimal(
            self.instance.time_delta.total_seconds() / 3600
        )
        if requested_payout > 0 and requested_payout > self._max_payout_hours():
            self.add_error(
                "payout_time_credit_hours",
                ValidationError(
                    _(
                        "You can't get payed out more hours than available (%(available)s)."
                    ),
                    code="invalid",
                    params={"available": self._max_payout_hours()},
                ),
            )
        if requested_payout < 0:
            self.add_error(
                "payout_time_credit_hours",
                ValidationError(
                    _("You can't get payed out negative values."), code="invalid"
                ),
            )

        # return 0 < requested_payout < balance + month_time_delta
        return min(requested_payout, max(balance + month_time_delta, 0))

    def clean(self):
        cleaned_data = self.cleaned_data
        employee = self.instance.employee
        month = self.instance.month
        open_time_entry = employee.monthly_time_entries(self.instance.month).filter(
            end__isnull=True
        )
        if open_time_entry.exists():
            raise ValidationError(
                _(
                    "There is still an open time entry on %(date)s. "
                    "Please close that first."
                ),
                code="open-timeentry",
                params={
                    "date": date_format(
                        timezone.localdate(open_time_entry.first().start)
                    ),
                },
            )

        if MonthlyTimeBilling.objects.filter(employee=employee, month=month).exists():
            raise ValidationError(
                _(
                    "Submission can't be saved: There is already a submitted time billing for this month."
                )
            )
        return cleaned_data


class HolidayPolicyForm(DependencyFormMixin, forms.ModelForm):
    class Meta:
        model = HolidayPolicy
        fields = [
            "name",
            "absence_type",
            "days_per_year",
            "holiday_expiration",
            "holiday_expiration_months",
            "is_active",
            "is_standard",
        ]
        field_dependencies = {"holiday_expiration": "#div_id_holiday_expiration_months"}

    def __init__(self, tenant, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.fields["absence_type"].queryset = AbsenceType.objects.filter(tenant=tenant)
        self.helper.layout = Layout(
            "name",
            "absence_type",
            "days_per_year",
            Row(
                Column("holiday_expiration"),
                Column(AppendedText("holiday_expiration_months", _(" months"))),
            ),
            "is_active",
            "is_standard",
        )
        # prohibit editing some fields of existing instances
        if self.instance.pk and self.instance.employees.exists():
            self.helper.layout.fields.insert(
                0,
                Alert(
                    _(
                        "There are employees using this policy. "
                        "Therefore some values are read-only."
                    ),
                    dismiss=False,
                    css_class="alert-warning",
                ),
            )
            self.fields["absence_type"].disabled = True
            self.fields["days_per_year"].disabled = True
            self.fields["holiday_expiration"].disabled = True
            self.fields["holiday_expiration_months"].disabled = True

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data["holiday_expiration"]:
            if (
                "holiday_expiration_months" not in self.cleaned_data
                or self.cleaned_data["holiday_expiration_months"] == 0
            ):
                self.add_error(
                    "holiday_expiration_months",
                    ValidationError(
                        _(
                            "If you activate 'Holiday expiration', you have to specify the number of months."
                        ),
                        code="required",
                    ),
                )
        return cleaned_data

    def on_changed_holiday_expiration(self, value):
        # if we have a pre-populated holiday_expiration field, disable the month field
        if value not in ["on", True]:
            field = self.fields["holiday_expiration_months"]
            field.disabled = True

    def get_update_url(self):
        if self.instance.pk:
            return reverse("timetracker:holidaypolicy:update", args=(self.instance.pk,))
        else:
            return reverse("timetracker:holidaypolicy:add")


class HolidayPolicyAddEmployeeForm(forms.ModelForm):
    class Meta:
        model = HolidayPolicyApplication
        fields = ["employee", "application_date"]

    application_date = forms.DateField(widget=forms.DateInput(attrs={"type": "date"}))

    def __init__(self, *args, **kwargs):
        self.holiday_policy = kwargs.pop("holiday_policy")
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.attrs.update(
            {
                "hx-post": reverse(
                    "timetracker:holidaypolicy:employee-add",
                    kwargs={"pk": self.holiday_policy.pk},
                )
            }
        )
        self.helper.layout = Layout(
            Row(
                Column("employee"),
                Column("application_date"),
                Column(Submit("submit", _("Save"), css_class="btn-primary")),
            ),
        )

        employees = self.fields["employee"]
        # exclude employees who are already assigned to this holiday policy
        employees.queryset = TimetrackerEmployee.objects.filter(
            tenant=self.holiday_policy.tenant
        ).exclude(pk__in=[e.pk for e in self.holiday_policy.employees.all()])

    def full_clean(self):
        super().full_clean()

    def clean(self):
        cleaned_data = super().clean()
        return cleaned_data

    def save(self, commit=True):
        application = super().save(commit=False)
        application.holiday_policy = self.holiday_policy
        return application.save()


class AbsenceTypeForm(forms.ModelForm):
    class Meta:
        model = AbsenceType
        fields = ("label", "color", "active", "needs_approval")

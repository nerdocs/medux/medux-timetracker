# Generated by Django 4.2.2 on 2023-08-25 20:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("timetracker", "0017_alter_monthlytimebilling_options_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="absencebalance",
            name="absence_type",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to="timetracker.absencetype",
            ),
        ),
    ]

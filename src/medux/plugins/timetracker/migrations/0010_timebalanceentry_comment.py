# Generated by Django 4.1.6 on 2023-04-03 19:47

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "timetracker",
            "0009_alter_timeentry_start_monthlytimebilling_payout_time_credit_hours_and_more",
        ),
    ]

    operations = [
        # already integrated in 0009
        #
        # migrations.AddField(
        #     model_name="timebalanceentry",
        #     name="comment",
        #     field=models.CharField(
        #         blank=True, max_length=255, null=True, verbose_name="Comment"
        #     ),
        # ),
    ]

from asgiref.sync import sync_to_async
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _

from medux.common.api.interfaces import IMenuItem, IActionButton

from medux.plugins.timetracker.models import TimeEntry


# def has_unfinished_timeentry(request):
#     """returns True if user is employee and has unfinished time slots"""
#     if request.user.is_anonymous or not request.user.is_employee:
#         return False
#     return TimeEntry.objects.filter(employee=request.employee, end=None).exists()


class TimetrackerDashboard(IMenuItem):
    menu = "views"
    title = _("Time tracker")
    icon = "calendar-range"
    url = reverse("timetracker:overview")


class Overview(IMenuItem):
    menu = "timetracker"
    title = _("Overview")
    icon = "house"
    url = reverse("timetracker:overview")


class TimeEntryList(IMenuItem):
    menu = "timetracker"
    title = _("Time entries")
    icon = "clock"
    url = reverse("timetracker:timeentry:month-list")


class Holidays(IMenuItem):
    menu = "timetracker"
    title = _("Holidays")
    icon = "airplane"
    url = reverse("timetracker:current-holidays")


class Archive(IMenuItem):
    menu = "timetracker"
    title = _("Archive")
    icon = "list"
    url = reverse("timetracker:archive")


@sync_to_async
def check_open_timeentry_available(request):
    """Helper to check if there is an open TimeEntry from today."""
    if not request.employee:
        return False
    return TimeEntry.objects.filter(
        employee=request.employee,
        # start__day=timezone.localdate(timezone.now()),
        end__isnull=True,
    ).exists()


class LogoutWithTimeTracking(IMenuItem):
    menu = "user"
    title = _("Logout with time tracking")
    url = "#"
    # url = reverse_lazy("timetracker:logout")
    weight = 90
    icon = "box-arrow-right"
    required_permissions = "timetracker.add_timeentry"
    check = check_open_timeentry_available
    attrs = {"hx-get": reverse_lazy("timetracker:logout"), "hx-target": "#dialog"}


class TimetrackerAdmin(IMenuItem):
    menu = "administration"
    slug = "ttadmin"
    title = _("Timetracker")
    url = "#"
    icon = "calendar-range"
    # required_permissions = "common.change_tenant"
    collapsed = False


class AdministerHolidayPolicies(IMenuItem):
    menu = "administration"
    slug = "ttadmin__holiday_rules"
    title = _("Holiday policies")
    icon = "umbrella"
    url = reverse_lazy("timetracker:holidaypolicy:list")
    required_permissions = "timetracker.change_tenant"


class PendingAbsenceRequests(IMenuItem):
    menu = "administration"
    slug = "ttadmin__pending_absence_requests"
    title = _("Pending absence requests")
    icon = "list-check"
    url = reverse_lazy("timetracker:pending-absence-requests")
    required_permissions = "timetracker.change_tenant"


class AbsenceTypeList(IMenuItem):
    menu = "administration"
    title = _("Absence types")
    slug = "ttadmin__absence_type"
    icon = "list"
    weight = 30
    url = reverse_lazy("timetracker:absencetype:list")


class ActionHolidayPolicyEmployees(IActionButton):
    menu = "holiday_policies_listitem"
    icon = "people"
    weight = 30
    view_name = "timetracker:holidaypolicy:employee-list"


class PendingAbsenceRequestActionApprove(IActionButton):
    menu = "absence_requests_listitem"
    icon = "check-lg"
    weight = 0
    view_name = "timetracker:absence:approve"
    method = "post"
    title = _("Approve absence request")


class PendingAbsenceRequestActionDecline(IActionButton):
    menu = "absence_requests_listitem"
    icon = "x-lg"
    weight = 5
    view_name = "timetracker:absence:decline"
    method = "post"
    title = _("Decline absence request")

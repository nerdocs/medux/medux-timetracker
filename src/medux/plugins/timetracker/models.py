import calendar
import logging
import typing
import uuid
import math

import holidays
from datetime import timedelta, datetime, date

from dateutil.relativedelta import relativedelta
from django.core.exceptions import ValidationError, ImproperlyConfigured
from django.urls import reverse
from holidays import WEEKEND

import django
from django.db import models
from django.db.models import TextChoices, Q, QuerySet
from django.utils import timezone
from django.utils.formats import time_format, date_format
from django.utils.translation import gettext_lazy as _
from month import Month
from month.models import MonthField

from medux.common.constants import UserColors
from medux.common.models import CreatedModifiedModel, Tenant
from medux.common.templatetags.medux import as_hours
from medux.core.models import PackageDataModel, PackageDataManager
from medux.employees.models import (
    WorkSchedule as EmployeeWorkSchedule,
    WorkingTimeRange,
    Employee,
    WorkingContract,
)
from medux.plugins.timetracker.constants import (
    ABSENCE_TYPE_STANDARD,
    ABSENCE_TYPE_PRESETS,
    ABSENCE_TYPE_ILLNESS,
)
from medux.preferences.definitions import Scope
from medux.preferences.models import ScopedPreference

logger = logging.getLogger(__name__)


def _roundTime(dt: datetime = None, dateDelta: timedelta = timedelta(minutes=1)):
    """Round a datetime object to a multiple of a timedelta

    @param dt: datetime.datetime object, default now.
    dateDelta : timedelta object, we round to a multiple of this, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
            Stijn Nevens 2014 - Changed to use only datetime objects as variables
            Christian González 2020 - adjustments and small improvements
    """
    roundTo = dateDelta.total_seconds()

    if dt is None:
        dt = django.utils.timezone.now()
    # Make sure dt and datetime.min have the same timezone
    tzmin = dt.min.replace(tzinfo=dt.tzinfo)

    seconds = (dt - tzmin).seconds
    rounding = (seconds + roundTo / 2) // roundTo * roundTo
    return dt + timedelta(0, rounding - seconds, -dt.microsecond)


class AbsenceStatus(TextChoices):
    NEW = ("new", _("New"))
    APPROVED = ("approved", _("Approved"))
    # CONSUMED = APPROVED and date < today
    DECLINED = ("declined", _("Declined"))


class AbsenceTypeIcons(TextChoices):
    UMBRELLA = "umbrella"
    PRESCRIPTION = "prescription2"
    AIRPLANE = "airplane"
    HOUSE = "house"


class AbsenceTypeManager(PackageDataManager):
    """Custom ObjectManager that prefills AbsenceTypes if a preset is created"""

    def create(self, *args, **kwargs) -> "AbsenceType":
        obj = super().create(*args, **kwargs)
        # check if uuid matches a preset.
        if "uuid" in kwargs:
            uuid = kwargs["uuid"]
            if uuid in ABSENCE_TYPE_PRESETS:
                # loop through preset dict
                for attr, value in ABSENCE_TYPE_PRESETS[uuid].items():
                    setattr(obj, attr, value)
                obj.save()
        return obj

    def create_as_vendor(self, *args, **kwargs):
        """This method must not be called."""
        raise NotImplementedError


class AbsenceType(PackageDataModel):
    """Absence types like holiday, illness etc."""

    class Meta:
        verbose_name = _("Absence type")
        verbose_name_plural = _("Absence types")
        unique_together = ("tenant", "uuid")

    # use a custom manager
    objects = AbsenceTypeManager()

    label = models.CharField(_("Label"), max_length=255)
    color = models.CharField(
        max_length=50, choices=UserColors.choices, default=UserColors.ORANGE
    )
    active = models.BooleanField(default=True)
    needs_approval = models.BooleanField(default=True)
    counts_as_negative_working_time = models.BooleanField(
        default=False,
        help_text=_(
            "if checked, this type is counted as negative hours for time accounting. If not, it is treated neutrally."
        ),
    )
    icon = models.CharField(
        _("Icon name"),
        choices=AbsenceTypeIcons.choices,
        max_length=50,
        default=AbsenceTypeIcons.UMBRELLA,
    )
    # override PackageDataModel and set a related_name
    tenant = models.ForeignKey(
        Tenant,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="absence_types",
        help_text=_("The tenant this absence type belongs to"),
    )

    def __str__(self):
        return self.label

    def translated_label(self):
        return _(self.label)

    def get_absolute_url(self):
        return reverse("timetracker:absencetype:detail", kwargs={"pk": self.pk})


class WorkSchedule(EmployeeWorkSchedule):
    """Proxy model for employees.WorkSchedule with timetracker features."""

    class Meta:
        proxy = True

    def clean(self):
        super().clean()
        # make sure a HolidayPolicy is applied to current user
        try:
            HolidayPolicyApplication.objects.get(
                employee=self.employee,
                application_date__lte=self.start_date,
            )
        except HolidayPolicyApplication.DoesNotExist:
            raise ValidationError(
                _(
                    "You need to apply a holiday policy to this user before you can create a work schedule."
                )
            )

    def recalculate_absence_entitlements(self, absence_type=None):
        """Recalculates absence entitlements for given absence_type.

        Attributes:
            absence_type: if None (default), absences entitlements are calculated for all absence types.
        """
        hes = self.holiday_entitlements.all()
        if not hes:
            self.create_absence_entitlements(absence_type)
            hes = self.holiday_entitlements.all()
        for he in hes:
            he.recalculate_absences()
            he.save()

    def create_absence_entitlements(self, absence_type=None) -> None:
        """Create holiday entitlements for this work schedule.

        Normally, this means creating one holiday entitlement with the same start
        and end date as the WorkSchedule. If the WorkSchedule crosses a new year/employment date anniversary,
        multiple holiday entitlements are created at these borders.
        If the work schedule is open-ended, only holiday entitlements are created until next leave year's end.

        Note that this does NOT calculate the absence sum of that entitlements, as there may be not all
        WorkingTimeRanges created yet for the WorkSchedule. You have to create them yourself using
        `recalculate_absence_entitlements()`

        Attributes:
            absence_type: If given, only create HEs for this absence types. If None, create them for all AbsenceTypes.
        Raises:
            `ValidationError` if the holiday entitlements could not be created.
        """

        if not self.pk:
            raise ValidationError(
                _(
                    "You have to save this work schedule before creating it's holiday entitlements!"
                )
            )
        employee: TimetrackerEmployee = TimetrackerEmployee.objects.get(
            pk=self.employee.pk
        )

        # if absence_type:
        #     if isinstance(absence_type, uuid.UUID):
        #         absence_type = AbsenceType.get_by_uuid(employee.tenant, absence_type)
        #
        # # get last valid holiday policy before reference day
        # policy = employee.get_valid_holiday_policy(
        #     reference_date=self.start_date, absence_type=absence_type
        # )
        # if not policy:
        #     # this can't be tolerated - calculation not possible.
        #     logger.info(
        #         _(
        #             "No holiday policy found for employee %(employee)s before %(day)s.".format(
        #                 employee=self.employee, day=self.start_date
        #             )
        #         ),
        #     )
        #     return

        # Always start at WS start
        start_date = self.start_date
        # determine the final date, until which to calculate the holiday entitlements
        next_leave_year_end = employee.next_leave_year_start_after(
            start_date, self.working_contract
        ) - timedelta(days=1)
        if self.end_date:
            final_date = self.end_date
        else:
            final_date = next_leave_year_end

        absence_types = None
        while start_date <= final_date:
            # if this WorkSchedule is open-ended, use current leave year-end date as final date.
            next_end_date = min(final_date, next_leave_year_end)

            # determine which absence types are needed to create, if not already done.
            if not absence_types:
                if absence_type:
                    if not isinstance(absence_type, AbsenceType):
                        absence_type = AbsenceType.get_by_uuid(
                            employee.tenant, absence_type
                        )
                    absence_types = [absence_type]
                else:
                    absence_types = self.employee.tenant.absence_types.filter(
                        counts_as_negative_working_time=False
                    ).exclude(uuid=ABSENCE_TYPE_ILLNESS)

            # create HEs for all available (or given) absence types
            for absence_type in absence_types:
                # calculate the holiday entitlements
                he, created = HolidayEntitlement.objects.get_or_create(
                    employee_id=self.employee.id,
                    work_schedule_id=self.id,
                    start_date=start_date,
                    end_date=next_end_date,
                    absence_type=absence_type,
                )
                # if created and policy.holiday_expiration:
                #     he.expired_at = self.start_date + relativedelta(
                #         months=policy.holiday_expiration_months
                #     )

            # determine next start date and leave year's end
            start_date = next_end_date + timedelta(days=1)
            next_leave_year_end = employee.next_leave_year_start_after(
                start_date, self.working_contract
            ) - timedelta(days=1)


class TimetrackerEmployee(Employee):
    """proxy model for Employee with timetracker features.

    Timetracker code should use this model instead of medux.employees.models.Employee
    """

    class Meta:
        proxy = True

    def apply_holiday_policy(self, policy: "HolidayPolicy", day: date) -> None:
        HolidayPolicyApplication.objects.create(
            employee=self, holiday_policy=policy, application_date=day
        )

    def daily_normal_working_time(self, day: date, wtrs_queryset=None) -> timedelta:
        """Return total normal working time for one day.

        if a WorkingTimeRange queryset is passed (for performance reasons), that one is used
        instead of self.working_time_ranges_for_day() to reduce DB calls.

        This method doesn't take into account absences, holidays, illness, public holidays etc.,
        it just returns the normal working time as if it would be a normal working day.
        """
        sum = timedelta()
        wtrs_queryset = wtrs_queryset or self.working_time_ranges_for_day(day)
        for wtr in wtrs_queryset:
            sum += wtr.duration()
        return sum

    def working_time_ranges_for_day(self, day: date) -> QuerySet[WorkingTimeRange]:
        """Returns WTR for given day.

        Does NOT care about (public/employee's holidays, illness, weekends etc),
        """
        try:
            ws = WorkSchedule.get_matching(employee=self, day=day)
            if ws:
                return ws.working_hours.filter(weekday=day.weekday())
        except WorkingTimeRange.DoesNotExist:
            pass
        return WorkingTimeRange.objects.none()

    def is_working_day(self, day: date, country=None) -> bool:
        """Returns True if given day is a working day.

        Avoid calling this function multiple times, as it is not very performant.

        Attributes:
            day: the day to check
            country: the country to check. if None, it will be auto-detected, but it will cost DB queries.
        """

        if not self.working_time_ranges_for_day(day).exists():
            return False

        if not country:
            country = ScopedPreference.get_effective(
                "common", "country", Scope.TENANT, tenant=self.tenant
            )

        # public holiday?
        if day in holidays.country_holidays(country, years=day.year):
            return False

        # employee's absence? (incl. illness)
        if Absence.objects.filter(
            employee=self,
            status=AbsenceStatus.APPROVED,
            day=day,
        ).exists():
            return False

        return True

    def daily_time_entry_sum(self, day: date) -> timedelta:
        """Sum up all (finished) time entries for given day.

        Open (unfinished) time entries are not considered.
        """
        entries = TimeEntry.objects.filter(
            employee=self,
            start__date=timezone.make_aware(
                timezone.datetime.combine(day, timezone.datetime.min.time()),
                timezone.get_current_timezone(),
            ),
            end__isnull=False,
        )
        sum = timedelta()
        for entry in entries:
            sum += entry.duration()
        return sum

    def monthly_normal_working_time(
        self, month: Month, count_absences=True
    ) -> timedelta:
        """Return summed up normal working time for given month.

        Attributes:
            month: the month to check
            count_absences: if True, days with absences or public holidays will be removed from the result sum
        """
        month_sum = timedelta()
        # loop through all days of month
        tenant_country = ScopedPreference.get(
            "common", "country", Scope.TENANT, tenant=self.tenant
        )
        # There MUST be a country defined for the tenant. At least by vendor.
        if not tenant_country:
            raise ImproperlyConfigured(
                "There is no country defined for tenant {}".format(self.tenant)
            )

        country_holidays = holidays.country_holidays(tenant_country, years=month.year)
        for day in range(1, calendar.monthrange(month.year, month.month)[1] + 1):
            current_date = date(month.year, month.month, day)

            if count_absences:
                # if there is a public holiday for this day, skip it
                if current_date in country_holidays:
                    continue
                # if there is an approved absence for this day (except CTO-like), skip it
                if (
                    Absence.objects.filter(
                        Q(employee=self),
                        Q(day=current_date),
                        Q(status=AbsenceStatus.APPROVED)
                        & Q(absence_type__counts_as_negative_working_time=False),
                    )
                    .values("id")
                    .exists()
                ):
                    continue

            day_wtrs = WorkingTimeRange.objects.filter(
                Q(work_schedule__employee=self),
                Q(work_schedule__start_date__lte=current_date),
                Q(work_schedule__end_date__isnull=True)
                | Q(work_schedule__end_date__gte=current_date),
                Q(weekday=current_date.weekday()),
            )
            for wtr in day_wtrs:
                month_sum += wtr.duration()
        return month_sum

    def monthly_time_entries(self, month: Month) -> QuerySet["TimeEntry"]:
        """Return total time entries for one month.

        Attributes:
            month: the month to be queried.
        """
        month_start = timezone.make_aware(
            datetime(
                month.year,
                month.month,
                1,
            )
        )
        month_end = month_start + relativedelta(months=1) - timedelta(seconds=1)

        return TimeEntry.objects.filter(
            Q(employee=self),
            Q(start__gte=month_start),
            Q(start__lte=month_end),
            Q(end__lte=month_end) | Q(end__isnull=True),
        )

    def monthly_time_entry_sum(self, month: Month) -> timedelta:
        """Return total worked time for complete month.

        Attributes:
            month: the month to be queried.
        """

        # this also includes open time entries, but their duration is 0
        entries = self.monthly_time_entries(month)
        month_sum = timedelta()
        for entry in entries:
            month_sum += entry.duration()
        return month_sum

    def last_leave_year_start_before(
        self, reference_day: date, contract: WorkingContract = None
    ) -> date | None:
        """Helper method that returns last employment date anniversary before reference date,
        or last Jan 1st, depending on user's holiday_year_is_calendar_year policy.

        Attributes:
            reference_day: the last employment date anniversary for this employee.
            contract: the contract for which HEs should be calculated.
                If contract is None, tries to get the active contract of reference day,
                If that is also not found, return None.
        """
        if not contract:
            contract = self.active_working_contract(reference_day)
            if not contract:
                return None

        if contract.holiday_year_is_calendar_year:
            # return first day of reference year
            return date(year=reference_day.year, month=1, day=1)
        else:
            # vacation year is not calendar year, but employment date is in this/next year
            hire_date = contract.employment_date
            this_years_employment_anniversary = hire_date.replace(
                year=reference_day.year
            )
            last_years_employment_anniversary = hire_date.replace(
                year=reference_day.year - 1
            )
            # if this year's employment anniversary is before reference date, return
            # if not, return last year's employment anniversary.
            return (
                this_years_employment_anniversary
                if this_years_employment_anniversary < reference_day
                else last_years_employment_anniversary
            )

    def next_leave_year_start_after(
        self, reference_day: date, contract: WorkingContract = None
    ) -> date | None:
        """Helper method that returns next employment date anniversary after reference date,
        or next calendar year-end day, depending on user's holiday_year_is_calendar_year
        preference.

        Attributes:
            reference_day: the next employment date anniversary for this employee.
            contract: the contract for which HEs should be calculated.
                If contract is None, returns None.
        """
        if not contract:
            contract = self.active_working_contract(reference_day)
            if not contract:
                return None

        if contract.holiday_year_is_calendar_year:
            # return first day of next after reference year
            return date(year=reference_day.year + 1, month=1, day=1)
            # FIXME: what to do on Jan 1st?
        else:
            # vacation year is not calendar year, but employment date is in this/next year
            hire_date = contract.employment_date
            this_years_employment_anniversary = hire_date.replace(
                year=reference_day.year
            )
            next_years_employment_anniversary = hire_date.replace(
                year=reference_day.year + 1
            )
        # if this year's employment anniversary is after reference date, return anniversary.
        # if not, return next year's employment anniversary.
        return (
            this_years_employment_anniversary
            if this_years_employment_anniversary > reference_day
            else next_years_employment_anniversary
        )

    def get_valid_holiday_policy(  # FIXME: get_last_holiday_policy_before
        self,
        reference_date: date = None,
        absence_type: AbsenceType | uuid.UUID | None = None,
        contract: WorkingContract = None,
    ) -> typing.Optional["HolidayPolicy"]:
        """get matching last HolidayPolicy that was applied to the employee
        before last leave year start, before reference_date, for active contract.

        (OMG, this is complicated.)
        """
        # TODO: maybe this should not be before last leave year start, but before reference date?
        if not reference_date:
            reference_date = timezone.now().date()
        if not contract:
            contract = self.active_working_contract(reference_date=reference_date)
        if not absence_type:
            absence_type = ABSENCE_TYPE_STANDARD
        if not isinstance(absence_type, AbsenceType):
            absence_type = AbsenceType.get_by_uuid(self.tenant, absence_type)

        last_leave_year_start = self.last_leave_year_start_before(
            reference_date + timedelta(days=1), contract  # include reference day!
        )
        if not last_leave_year_start:
            return None

        hpas = HolidayPolicyApplication.objects.filter(
            employee_id=self.id,
            application_date__lte=last_leave_year_start,
            holiday_policy__absence_type=absence_type,
        ).order_by("application_date")
        if not hpas:
            return None
        return hpas.last().holiday_policy

    def create_holiday_entitlements(
        self,
        contract: WorkingContract,
        absence_type=None,
    ):
        """Create (missing) HolidayEntitlement objects for all work schedules of employee's contract.

        Attributes:
            contract: the contract to calculate the holiday entitlements for.
            absence_type: the absence type to calculate the holiday entitlements for. If None, use all absence types.
        """
        for ws in contract.work_schedules.all():
            # cast WorkSchedule to timetracker's
            ws.__class__ = WorkSchedule
            ws.create_absence_entitlements(absence_type=absence_type)

    def absences_count(
        self,
        from_date: date = None,
        to_date: date = None,
        absence_type=None,
        absence_status=AbsenceStatus.APPROVED,
        only_current_leave_year=True,
        contract: WorkingContract = None,
    ) -> int:
        """Returns number all past consumed absences for given point in time,
        optionally only given absence_type.

        Attributes:
            from_date: the first date that should include absences. If
                only_current_leave_year=True, this is used as reference date for current
                leave year.
            to_date: the last day when absences occur.
            absence_type: the type of absences to find. If none, query for all.
            absence_status: the status of absences searched for. Default: APPROVED
            only_current_leave_year: if True (default), search for absences only in
                current leave year (in this case the from_date parameter is used as
                reference date to determine the leave year).
            contract: the current WorkingContract. This can be passed if available
                to reuse it and reduce DB queries.
        """

        filter = {}
        today = timezone.now().date()
        if absence_type:
            if isinstance(absence_type, uuid.UUID):
                absence_type = AbsenceType.get_by_uuid(self.tenant, absence_type)
            filter["absence_type"] = absence_type

        if only_current_leave_year and to_date:
            raise AttributeError(
                "You can either use from_date/to_date OR "
                "only_current_leave_year, not both."
            )
        if only_current_leave_year:
            # redefine from_date/to_date to begin/end of leave year, using from_date
            # parameter as reference date
            assert not to_date

            if not from_date:
                from_date = today

            if not contract:
                contract = self.active_working_contract(reference_date=from_date)
                if not contract:
                    return 0

            from_date = max(
                self.last_leave_year_start_before(
                    from_date + timedelta(days=1), contract=contract
                ),
                contract.start_date,
            )

            to_date = self.next_leave_year_start_after(
                reference_day=from_date, contract=contract
            ) - timedelta(days=1)
            filter["day__range"] = (from_date, to_date)
        else:
            if from_date:
                if to_date:
                    filter["day__range"] = (from_date, to_date)
                else:
                    to_date = today
                    filter["day__gte"] = to_date
                if from_date > to_date:
                    raise ValueError("from_date can not be after to_date/now.")
            else:
                if to_date:
                    filter["day__lte"] = to_date
                else:
                    filter["day__lte"] = today

        return Absence.objects.filter(
            employee_id=self.id,
            status=absence_status,
            **filter,
        ).count()

    def estimated_absence_entitlement(
        self,
        reference_date: date = None,
        absence_type: AbsenceType | uuid.UUID = ABSENCE_TYPE_STANDARD,
        contract: WorkingContract = None,
    ) -> float:
        """Absence entitlement estimated until the end of current leave year."""

        start = self.last_leave_year_start_before(
            reference_date + timedelta(days=1), contract
        )
        end = self.next_leave_year_start_after(reference_date, contract) - timedelta(
            days=1
        )
        if type(absence_type) == uuid.UUID:
            absence_type = AbsenceType.get_by_uuid(self.tenant, absence_type)
        hes = self.holiday_entitlements.filter(
            absence_type=absence_type, start_date__lte=start, end_date__gte=end
        )
        sum = 0.0
        for he in hes:
            sum += he.holidays
        return sum

    def absence_entitlement(
        self,
        reference_date: date = None,
        absence_type: AbsenceType | uuid.UUID = ABSENCE_TYPE_STANDARD,
        contract: WorkingContract = None,
    ) -> float:
        """Returns the current holiday entitlement at given point in time.

        Attributes:
            absence_type: the AbsenceType of the contingent.
            reference_date: all holidays before that date are taken into account for the calculation.
                If unset, "today" is assumed.
        """
        if not contract:
            contract = self.active_working_contract(reference_date)
        if not reference_date:
            reference_date = timezone.now().date()
        if type(absence_type) == uuid.UUID:
            absence_type = AbsenceType.get_by_uuid(self.tenant, absence_type)

        policy = self.get_valid_holiday_policy(reference_date, absence_type)
        if not policy:
            return 0
        return policy.days_per_year

    def absence_balance(
        self,
        reference_date: date = None,
        absence_type: uuid.UUID | AbsenceType = ABSENCE_TYPE_STANDARD,
    ) -> int:
        """returns calculated total consumed absences for absence type and given point in time,
        even with different work schedules.

        The resulting number is always rounded up.

        Attributes:
            reference_date: all absences before that date are taken into account for the calculation.
            absence_type: the type of absences to take into account. Defaults to ABSENCE_TYPE_STANDARD.
                If set to `None`, all absence types are summed up. TODO.
        """

        if not reference_date:
            reference_date = timezone.now().date()
        if type(absence_type) == uuid.UUID:
            absence_type = AbsenceType.get_by_uuid(self.tenant, absence_type)

        sum = 0.0
        hes = HolidayEntitlement.objects.filter(
            employee_id=self.id,
            start_date__lte=reference_date,
            absence_type=absence_type,
        )
        for he in hes:
            sum += he.holidays

        consumed_absences = self.absences_count(
            to_date=reference_date,
            absence_type=absence_type,
            only_current_leave_year=False,
        )

        return math.ceil(sum) - consumed_absences

    def time_account_balance(self) -> timedelta:
        """Returns current balance of time account"""
        return TimeBalanceEntry.objects.filter(employee=self).last().balance


class TimeEntry(CreatedModifiedModel):
    """A time entry for a specific block of time, of one person."""

    # TODO: Schedules must not overlap
    # TODO: time entries of one user must not overlap
    # validate time entries:
    #   TODO: start ! >= end

    class Meta:
        verbose_name = _("Time entry")
        verbose_name_plural = _("Time entries")

    employee = models.ForeignKey(TimetrackerEmployee, on_delete=models.CASCADE)
    work_schedule = models.ForeignKey(
        WorkSchedule, on_delete=models.PROTECT, null=True, related_name="time_entries"
    )
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    comment = models.CharField(
        max_length=255, blank=True, help_text=_("Optional comment for this time entry")
    )
    cached_delta = models.DurationField(_("Delta"), blank=True, null=True)

    def is_weekend(self):
        return self.start.weekday() in WEEKEND

    def duration(self) -> timedelta:
        """Returns the duration of the time entry as timedelta.

        If no end is given, return an empty timedelta.
        """
        if self.end:
            return self.end - self.start
        assert timezone.is_aware(self.start)  # DEBUG
        return timedelta()

    def duration_h(self) -> float:
        return self.duration().total_seconds() / 60 / 60

    def clean(self):
        if self.start:
            try:
                MonthlyTimeBilling.objects.get(
                    employee=self.employee, month=self.start.date().replace(day=1)
                )
                raise ValidationError(
                    _(
                        "{month} {year} is already submitted. "
                        "It is not possible to add time entries for it."
                    ).format(
                        month=_(calendar.month_name[self.start.month]),
                        year=self.start.year,
                    ),
                )
            except MonthlyTimeBilling.DoesNotExist:
                pass
            if self.end:
                if self.end < self.start:
                    raise ValidationError(
                        {"end": _("End time cannot be before start time.")},
                        code="invalid",
                    )
                if self.end == self.start:
                    raise ValidationError(
                        {"end": _("End time cannot be same as start time.")},
                        code="invalid",
                    )

                # check for overlapping time entries
                overlapping = TimeEntry.objects.filter(
                    Q(employee=self.employee),
                    Q(start__lt=self.end),
                    Q(end__isnull=True) | Q(end__gt=self.start),
                ).exclude(pk=self.pk)
                if overlapping.exists():
                    raise ValidationError(
                        _(
                            "This time entry overlaps with another one: '{"
                            "overlapping}'."
                        ).format(overlapping=overlapping.first())
                    )

    def __str__(self):
        if self.end:
            end = time_format(timezone.localtime(self.end).time())
        else:
            end = _("now")
        return f"{date_format(self.start)} {time_format(timezone.localtime(self.start))} - {end}"

    def save(self, **kwargs):
        """Set work_schedule to employee's active one at that time if not provided."""
        # FIXME: rounding is dependent on current tenant, hence must be done in view.
        #     if PreferencesRegistry.get["ROUND"]:
        #         round = settings.TIMETRACKER["ROUND"]
        #         self.start = _roundTime(self.start, timedelta(minutes=round))
        #         if self.end:
        #             self.end = _roundTime(self.end, timedelta(minutes=round))
        #   self.delta(recalculate=True)

        if not self.work_schedule:
            self.work_schedule = WorkSchedule.get_matching(self.employee, self.start)
        return super().save(**kwargs)

    # def wtr(self) -> WorkingTimeRange | None:
    #     """returns WorkingTimeRange that matches this TimeEntry."""
    #
    #     # get a list ot WorkingTimeRanges for this day
    #     working_hours = self.work_schedule.working_hours.filter(
    #         weekday=self.start.weekday()
    #     )
    #     # if no working_hours are defined yet, just return empty WTR
    #     if not working_hours:
    #         return None
    #
    #     thetime = self.start
    #     match = (
    #         working_hours.filter(start_time__lte=thetime, end_time__gt=thetime)
    #         .order_by("-start_time")
    #         .first()
    #     )
    #     if not match:
    #         next_wtr = (
    #             working_hours.filter(start_time__gt=thetime)
    #             .order_by("start_time")
    #             .first()
    #         )
    #         prev_wtr = (
    #             working_hours.filter(end_time__lte=thetime)
    #             .order_by("-end_time")
    #             .first()
    #         )
    #         if next_wtr and prev_wtr:
    #             if (thetime - prev_wtr.end).total_seconds() < (
    #                 next_wtr.start - thetime
    #             ).total_seconds():
    #                 match = prev_wtr
    #             else:
    #                 match = next_wtr
    #         elif next_wtr:
    #             # there is only a next block, no previous
    #             match = next_wtr
    #         else:
    #             # there is only a previous block, and no next one.
    #             match = prev_wtr
    #     return match

    # def delta(self, recalculate=False) -> timedelta:
    #     """returns calculated delta between current duration and scheduled duration.
    #
    #     Note:
    #         the WTR (incl. cached_delta) is not saved in this method. This must be done manually.
    #     """
    #     if recalculate or not self.cached_delta:
    #         wtr = self.wtr()
    #         if not wtr:
    #             return timedelta()
    #         self.cached_delta = self.duration() - wtr.duration()
    #     return self.cached_delta

    # def delta_h(self) -> float:
    #     delta = self.delta()
    #     if delta.days < 0:
    #         sign = -1
    #     else:
    #         sign = 1
    #
    #     total_seconds = abs(delta.seconds)
    #     return total_seconds / 60 / 60 * sign
    #     # hours = total_seconds // 3600
    #     # minutes = (total_seconds % 3600) // 60
    #     # return f"{sign}{hours}:{minutes}"


class OnlyNeutralAbsencesManager(models.Manager):
    # noinspection PyMethodMayBeStatic
    def get_queryset(self):
        """return all objects that count for time accounting."""
        return (
            super()
            .get_queryset()
            .filter(absence_type__counts_as_negative_working_time=False)
        )


class Absence(models.Model):
    """Represents one absence day for one user, be it holidays, CTO or illness, or any other custom absence type."""

    class Meta:
        verbose_name = _("Absence")
        verbose_name_plural = _("Absences")
        unique_together = ["day", "employee", "absence_type"]
        permissions = (
            (
                "administer_absence",
                _("Can administer absences"),
            ),
        )

    # as default, don't return CTO Absences!
    objects = models.Manager()
    only_neutral = OnlyNeutralAbsencesManager()

    day = models.DateField(verbose_name=_("Date"))
    employee = models.ForeignKey(
        TimetrackerEmployee, verbose_name=_("Employee"), on_delete=models.CASCADE
    )
    absence_type = models.ForeignKey(
        AbsenceType,
        verbose_name=_("Type"),
        on_delete=models.PROTECT,
        null=True,
    )
    status = models.CharField(
        max_length=25, choices=AbsenceStatus.choices, default=AbsenceStatus.NEW
    )

    def __str__(self):
        return (
            f"{self.employee}: {date_format(self.day,'SHORT_DATE_FORMAT')} ({self.absence_type}) "
            f"[{self.status}]"
        )

    def daily_normal_working_time(self):
        return self.employee.daily_normal_working_time(self.day)

    def collisions(self) -> QuerySet["Absence"]:
        """Returns a list of this employee's absences that overlap with this one."""
        return self.objects.filter(employee=self.employee, day=self.day).exclude(
            pk=self.pk
        )

    def save(self, **kwargs):
        if not self.absence_type:
            self.absence_type = AbsenceType.get_by_uuid(
                self.employee.tenant, ABSENCE_TYPE_STANDARD
            )
        super().save(**kwargs)

        # if absence is approved, delete all TimeEntries on that day
        if self.status == AbsenceStatus.APPROVED:
            TimeEntry.objects.filter(
                employee=self.employee, start__date=self.day
            ).delete()


class HolidayEntitlement(CreatedModifiedModel):
    """The holiday entitlement for one WorkSchedule. or (max.) a "vacation year".

    A holiday year counts from/to employment_date anniversary, or matches
    the calendar year, depending on the user's holiday_year_is_calendar_year policy.
    """

    class Meta:
        verbose_name = _("Holiday entitlement")
        verbose_name_plural = _("Holiday entitlements")
        ordering = ["start_date"]

    employee = models.ForeignKey(
        TimetrackerEmployee,
        on_delete=models.CASCADE,
        related_name="holiday_entitlements",
    )
    work_schedule = models.ForeignKey(
        WorkSchedule, on_delete=models.CASCADE, related_name="holiday_entitlements"
    )
    start_date = models.DateField(_("Start date"))
    end_date = models.DateField(_("End date"))
    expired_at = models.DateField(_("Expiration date"), blank=True, null=True)
    holidays = models.FloatField(
        _("Holiday entitlement"),
        default=0,
        help_text=_("Holiday entitlement in days"),
    )
    absence_type = models.ForeignKey(AbsenceType, on_delete=models.PROTECT, null=True)

    def clean(self):
        # TODO: make this two years dynamic
        # This depends on local laws, and preferences of tenant.
        ScopedPreference.get_effective(
            "timetracker", "holidays_expiration", Scope.TENANT
        )
        # FIXME: in Germany, it's March 31th of the following year. Be more flexible
        self.expired_at = self.start_date.replace(year=self.start_date.year + 2)

    def __str__(self) -> str:
        return f"{self.start_date} - {self.end_date} [{self.absence_type}]: {self.holidays}"

    def save(self, *args, **kwargs):
        """recalculates absences and saves the object."""
        self.recalculate_absences()
        super().save(*args, **kwargs)

    def recalculate_absences(self) -> None:
        """Recalculate absence entitlements.

        It just does the calculation and saves the result in self.holidays.
        However, it does NOT save the object, please to that yourself."""

        work_schedule = self.work_schedule
        if not work_schedule:
            self.holidays = 0
            return

        days_per_week = work_schedule.work_days_per_week()
        if not days_per_week:
            self.holidays = 0
            return

        # get last valid holiday policy for this user
        policy = self.employee.get_valid_holiday_policy(
            reference_date=self.start_date,
            absence_type=self.absence_type,
        )
        if not policy:
            self.holidays = 0
            return

        # if not absolute, use proportional part
        if policy.is_absolute:
            self.holidays = policy.days_per_year
        else:
            self.holidays = policy.days_per_year / 6 * days_per_week

        self.holidays = (
            self.holidays / 365 * ((self.end_date - self.start_date).days + 1)
        )


def current_year() -> int:
    """helper to get current year as default value"""
    return timezone.now().year


class MonthlyTimeBilling(CreatedModifiedModel):
    """A bunch of TimeEntries of one month, together with some metadata, that is submitted
    by an employee"""

    class Meta:
        verbose_name = _("Monthly time billing")
        verbose_name_plural = _("Monthly time billings")
        unique_together = ["employee", "month"]
        ordering = ["month"]

    employee = models.ForeignKey(
        TimetrackerEmployee, on_delete=models.CASCADE, related_name="monthsubmissions"
    )

    # only year and month are used from month field, day always is 1, when converted to date
    month = MonthField(_("Month"), help_text=_("The month you want to submit"))

    payout_time_credit_hours = models.DecimalField(
        _("Amount of hours to pay out"),
        max_digits=4,
        decimal_places=2,
        help_text=_("All remaining time will get shifted to your time credit balance."),
        default=0,
    )
    # This field can only be set by a tenant admin
    payout_month = MonthField(_("Payout in this month"), blank=True, null=True)

    # cached data
    time_targeted = models.DurationField(_("Target time"))
    time_worked = models.DurationField(_("Time worked"))

    @property
    def time_delta(self):
        """Return the delta time of the current month"""
        return self.time_worked - self.time_targeted

    def recalculate(self):
        if self.employee:
            if isinstance(self.employee, Employee):
                # FIXME: AARGH, bad hack. Don't read the following line, it's bad for your eyes.
                self.employee.__class__ = TimetrackerEmployee
                # but if not done this way, self.employee always returns an Employee,
                # not a TimetrackerEmployee...?!? I don't know why.

            self.time_targeted = self.employee.monthly_normal_working_time(self.month)
            self.time_worked = self.employee.monthly_time_entry_sum(self.month)

    def __str__(self):
        return f"{self.employee}, {self.month}"


class TimeBalanceEntry(CreatedModifiedModel):
    """An entry of an employee's time account.

    It recalculates the current balance with each entry.
    """

    class Meta:
        verbose_name = _("Time balance")
        verbose_name_plural = _("Time balance entries")
        ordering = ["created"]

    employee = models.ForeignKey(TimetrackerEmployee, on_delete=models.CASCADE)
    time_delta = models.DurationField(_("Time delta"), default=timezone.timedelta())

    # this field shouldn't be set directly, as it is calculated by adding a time_delta
    balance = models.DurationField(_("Balance"), blank=True)
    time_billing = models.OneToOneField(
        MonthlyTimeBilling,
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="time_balance_entry",
    )
    comment = models.CharField(_("Comment"), max_length=255, blank=True, null=True)

    def save(self, **kwargs):
        # get last entry before this one, if exists
        latest = (
            TimeBalanceEntry.objects.filter(
                employee=self.employee, created__lt=self.created
            )
            .order_by("created")
            .last()
        )
        if latest:
            self.balance = latest.balance + self.time_delta
        super().save(**kwargs)

    def __str__(self):
        return (
            f"{self.employee}: {date_format(self.created)}, {time_format(self.created)} -"
            f" {as_hours(self.balance)}"
        )


class AbsenceBalance(CreatedModifiedModel):
    """An entry of an employee's absence account, at time of leave year start (employment_date/ Jan 1st).

    This is basically the carry-over leave days from the year before that date.
    The date of the balance must be the leave year start date, which is either Jan 1st, or an employment date
    anniversary, depending on the tenant preference.
    """

    class Meta:
        verbose_name = _("Absence balance")
        verbose_name_plural = _("Absence balance entries")
        ordering = ["created"]
        unique_together = ["employee", "absence_type", "day"]

    employee = models.ForeignKey(TimetrackerEmployee, on_delete=models.CASCADE)
    absence_type = models.ForeignKey(AbsenceType, on_delete=models.CASCADE)
    day = models.DateField(_("Date"))
    balance = models.FloatField(_("Balance"), blank=True)
    comment = models.CharField(_("Comment"), max_length=255, blank=True, null=True)

    def __str__(self):
        return f"{self.employee}: {date_format(self.created)}, {time_format(self.created)} - {self.balance}"


class HolidayPolicyApplication(models.Model):
    class Meta:
        unique_together = ["employee", "holiday_policy", "application_date"]

    employee = models.ForeignKey(TimetrackerEmployee, on_delete=models.CASCADE)
    holiday_policy = models.ForeignKey("HolidayPolicy", on_delete=models.CASCADE)
    application_date = models.DateField(
        _("Application date"), help_text=_("The date the rule applies")
    )

    def __repr__(self):
        return f"<class {self.__class__.__name__}>"

    def __str__(self):
        return f"{self.employee} - {self.application_date} - {self.holiday_policy}"


class HolidayPolicy(CreatedModifiedModel):
    """A rule for creation of holidays, applicable to one or more employees.

    A holiday rule is a rule that is applied to every new holiday year of the employee.
    """

    class Meta:
        verbose_name = _("Holiday policy")
        verbose_name_plural = _("Holiday policies")
        ordering = ["name"]

    tenant = models.ForeignKey(Tenant, on_delete=models.CASCADE)
    employees = models.ManyToManyField(
        TimetrackerEmployee,
        verbose_name=_("Employees using this policy"),
        related_name="holiday_policies",
        through=HolidayPolicyApplication,
    )
    name = models.CharField(_("Name"), max_length=255)
    days_per_year = models.PositiveSmallIntegerField(
        default=30,
        help_text=_("Holidays per year, when a 6-days week is assumed."),
    )
    absence_type = models.ForeignKey(
        AbsenceType,
        verbose_name=_("Type"),
        on_delete=models.PROTECT,
    )
    holiday_expiration = models.BooleanField(
        _("Holidays expire"),
        default=False,
        help_text=_("Holidays expire after time."),
    )
    holiday_expiration_months = models.PositiveSmallIntegerField(
        _("Holidays expire after"),
        default=0,
        help_text=_("Holidays expire these months after they are created."),
        blank=True,
    )

    is_active = models.BooleanField(
        _("Active"), default=True, help_text=_("Uncheck this to hide this policy.")
    )
    is_standard = models.BooleanField(
        _("Standard"),
        help_text=_("This is the standard policy for this tenant."),
        default=False,
    )
    is_absolute = models.BooleanField(
        _("Absolute days"),
        help_text=_(
            "If checked, days are absolute, and not calculated proportionately controlled by employment percentage."
        ),
        default=False,
    )

    def clean(self):
        # make sure that if holiday_expiration is activated, holiday_expiration_months has a value
        if not self.holiday_expiration_months:
            self.holiday_expiration_months = 0

    def apply_to_employee(self, employee: Employee, application_date: date):
        hpa = HolidayPolicyApplication(
            employee=employee,
            holiday_policy=self,
            application_date=application_date,
        )
        hpa.save()
        pass

    def save(self, **kwargs):
        """Saves this policy.

        If this is a standard policy, remove standard attribute from tenant's all other policies.
        """
        # specifying a default `per tenant` is not possible as a function. We have to do this
        # here in the save() method.
        if not self.absence_type:
            self.absence_type = AbsenceType.get_by_uuid(
                self.tenant, ABSENCE_TYPE_STANDARD
            )
        others = HolidayPolicy.objects.filter(
            tenant=self.tenant, absence_type=self.absence_type
        )
        # if updating, we have a pk:
        if self.pk:
            others = others.exclude(pk=self.pk)
        if self.is_standard:
            # set all other (same tenant's) policies to non-standard
            others.update(is_standard=False)
        elif not others.exists():
            self.is_standard = True

        super().save(**kwargs)

    def delete(self, **kwargs):
        """Deletes this policy, sets first of other tenant's policies to standard."""
        others_first = (
            HolidayPolicy.objects.filter(
                tenant=self.tenant, absence_type=self.absence_type
            )
            .exclude(pk=self.pk)
            .first()
        )
        if others_first:
            others_first.is_standard = True
            others_first.save()

        super().delete(**kwargs)

    def __str__(self):
        return self.name

    def employees_list(self):
        return ", ".join([str(e) for e in self.employees.all()])

    def last_application_date(self, employee):
        return (
            self.holiday_policyapplication_set.filter(employee=employee)
            .order_by("application_date")
            .last()
            .application_date
        )

(()=>{
//TODO: extract duplicate code here
  function increaseTime(event) {
    let timeInput = this.parentElement.querySelector('input')
    let units = parseInt(timeInput.attributes["data-units"].value)
    let timeParts = timeInput.value.split(':')
    let hours = parseInt(timeParts[0])
    let minutes = parseInt(timeParts[1])
    minutes = Math.floor(minutes / units) * units + units
    if (minutes >= 60) {
      hours += 1
      minutes -= 60
    }
    if (hours >= 24) {
      hours = 0
    }
    if (minutes < 10) {
      minutes = '0' + minutes
    }
    if (hours < 10) {
      hours = '0' + hours;
    }
    timeInput.value = hours + ':' + minutes
  }

  function decreaseTime(event) {
    let timeInput = this.parentElement.querySelector('input')
    let units = parseInt(timeInput.attributes["data-units"].value)
    let timeParts = timeInput.value.split(':')
    let hours = parseInt(timeParts[0])
    let minutes = parseInt(timeParts[1])
    minutes = Math.ceil(minutes / units) * units - units
    if (minutes < 0) {
      hours -= 1;
      minutes += 60
    }
    if (hours < 0) {
      hours = 23
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    if (hours < 10) {
      hours = '0' + hours;
    }
    timeInput.value = hours + ':' + minutes;
  }

  document.querySelectorAll(".increase-time-button").forEach((element) =>  {
    element.addEventListener('click', increaseTime);
  })
  document.querySelectorAll(".decrease-time-button").forEach((element) =>  {
    element.addEventListener('click', decreaseTime);
  })

})();
import calendar
from datetime import datetime


from crispy_forms.layout import Layout, Row, Column
from django import forms
from django.core.exceptions import ValidationError
from django.forms import Form, SelectDateWidget
from django.utils import timezone
from django.utils.formats import time_format, date_format
widget = SelectDateWidget

from django.utils.translation import gettext_lazy as _

from medux import notifications
from medux.common.api.interfaces import ILoginViewExtension, ILoginFormExtension
from medux.common.bootstrap import RadioSelectButtonsGroup
from .bootstrap import TimeSelector
from medux.common.tools import round_down_time
from medux.plugins.timetracker.models import (
    TimeEntry,
    TimetrackerEmployee,
    MonthlyTimeBilling,
)

COME_GO_CHOICES = (
    ("come", _("Coming")),
    ("go", _("Going")),
)


class TimetrackerLoginFormExtension(ILoginFormExtension):
    """Provides simplified start/end time fields at the login form."""

    class Media:
        js = ("timetracker/js/timeselector.js",)

    field_names = ["log_time", "come_go"]

    def alter_fields(self, fields: dict):
        """add log_time and come/go fields to Login dialog"""
        fields["log_time"] = forms.TimeField(
            label=_("Work start/end time"),
            required=False,
            initial=time_format(round_down_time(timezone.localtime(), 15)),
            help_text=_(
                "If you select 'Come' or 'Go' here, the time will be logged as working start/end time."
            ),
        )
        fields["come_go"] = forms.ChoiceField(
            label=False,
            required=False,
            choices=COME_GO_CHOICES,
        )

    def alter_layout(self, layout: Layout):
        """insert a timeselector and come/go fields into layout"""
        fields: list = layout.fields[0].fields
        fields.insert(
            2,
            Row(
                Column(
                    TimeSelector("log_time"),
                    RadioSelectButtonsGroup("come_go"),
                ),
            ),
        )

    def clean(self, host_form: Form) -> dict:
        cleaned_data = host_form.cleaned_data

        # if no come/go decicion is taken, ignore the rest
        if cleaned_data["come_go"] not in ["come", "go"]:
            return cleaned_data

        employee = TimetrackerEmployee.objects.get(username=cleaned_data["username"])
        beginning_of_today = timezone.localtime(timezone.now()).replace(
            hour=0, minute=0, second=0, microsecond=0
        )
        open_time_entry = TimeEntry.objects.filter(
            employee=employee,
            end__isnull=True,
        )
        open_time_entry_exists = open_time_entry.exists()
        if cleaned_data["come_go"] == "come":
            if open_time_entry_exists:
                host_form.add_error(
                    "log_time",
                    ValidationError(
                        _(
                            "There is already an active time entry on {date}, at {time}. Please close this "
                            "first.",
                        ).format(
                            date=date_format(
                                timezone.localdate(open_time_entry.first().start)
                            ),
                            time=time_format(
                                timezone.localtime(open_time_entry.first().start)
                            ),
                        ),
                        code="invalid",
                    ),
                )
        elif cleaned_data["come_go"] == "go":
            if len(open_time_entry) != 1:
                host_form.add_error(
                    "log_time",
                    ValidationError(
                        _(
                            "There are more than one unfinished time entries. Please contact your administrator."
                        ),
                        code="system-error",
                    ),
                )
                return cleaned_data

            open_time_entry_before_today = open_time_entry.filter(
                start__lt=beginning_of_today
            )
            if open_time_entry_before_today.exists():
                host_form.add_error(
                    "log_time",
                    ValidationError(
                        _(
                            "There is an active time entry, but not today, but at {date}. "
                            "Please close this first.",
                        ).format(
                            date=date_format(open_time_entry_before_today.first().start)
                        ),
                        code="invalid",
                    ),
                )
                return cleaned_data

                # FIXME: This code violates DRY: TimeEntryForm.clean* methods. Deduplicate!
                open_time_entry_today = open_time_entry.filter(
                    start__gte=beginning_of_today
                )
                open_time_entry_today = open_time_entry_today.first()
                timestamp = timezone.make_aware(
                    datetime.combine(timezone.now().date(), cleaned_data["log_time"])
                )
                if timestamp == open_time_entry_today.start:
                    host_form.add_error(
                        "log_time",
                        ValidationError(
                            _("End time cannot be same as start time."),
                            code="end-not-same-start",
                        ),
                    )
                if timestamp < open_time_entry_today.start:
                    host_form.add_error(
                        "log_time",
                        ValidationError(
                            _(
                                "End time cannot be before start time ({start_time})."
                            ).format(
                                start_time=time_format(open_time_entry_today.start)
                            ),
                            code="end-not-before-start",
                        ),
                    )

                first_of_month = timestamp.date().replace(day=1)
                if MonthlyTimeBilling.objects.filter(
                    employee=employee, month=first_of_month
                ).exists():
                    host_form.add_error(
                        None,
                        ValidationError(
                            _(
                                "{month} {year} is already submitted. "
                                "It is not possible to add time entries for it."
                            ).format(
                                month=_(calendar.month_name[timestamp.month]),
                                year=timestamp.year,
                            ),
                            code="invalid",
                        ),
                    )
        return cleaned_data


class TimetrackerLoginViewExtension(ILoginViewExtension):
    def form_valid(self, form, response) -> None:
        """After successfully logging in, save some timetracker data."""
        if not form.cleaned_data["come_go"]:
            return
        today = datetime.date(datetime.today())
        try:
            employee = TimetrackerEmployee.objects.get(
                username=form.cleaned_data["username"]
            )
            timestamp = timezone.make_aware(
                datetime.combine(today, form.cleaned_data["log_time"])
            )
            open_time_entry = TimeEntry.objects.filter(
                employee=employee, end__isnull=True
            )
            if form.cleaned_data["come_go"] == "come":
                TimeEntry.objects.create(employee=employee, start=timestamp)
            elif form.cleaned_data["come_go"] == "go":
                if open_time_entry.exists():
                    open_time_entry = open_time_entry.first()
                    if open_time_entry.start < timestamp:
                        open_time_entry.end = timestamp
                        open_time_entry.save()
                    else:
                        notifications.error(
                            _("End time cannot be before start time."),
                            recipient=employee,
                        )  # TODO

        except TimetrackerEmployee.DoesNotExist:
            pass

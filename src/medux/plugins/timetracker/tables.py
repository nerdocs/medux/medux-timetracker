from collections import OrderedDict
from datetime import datetime, timedelta
from gettext import ngettext

from django.db.models.enums import IntegerChoices
from django.utils.translation import gettext as _, gettext_lazy as _

import django_tables2 as tables
from django.urls import reverse
from django.utils.html import format_html
from month import Month

from .models import (
    HolidayPolicy,
    Absence,
    AbsenceType,
    MonthlyTimeBilling,
    TimeBalanceEntry,
)
from medux.common.htmx import HxLink, HxActionButton, HxButton
from medux.common.tables import ActionButtonsColumn
from medux.common.api.interfaces import IActionButton
from medux.common.templatetags.medux import month_name, as_hours, prefix_plus


class ActionButtonType(IntegerChoices):
    """Action buttons type, and their weights."""

    EDIT = 10
    DELETE = 20
    DELETE_WITH_CONFIRM = 21


class ActionButtonsTable(tables.Table):
    """A table mixin that provides an "actions" column.

    Per default, the `actions` column is positioned at the end via Meta.sequence. However, if you use your own `Meta`
    class for configuring the table (which you most probably will), you have to provide a `sequence=("...", "actions")`
    yourself.
    """

    actions = ActionButtonsColumn()

    class Meta:
        # WARNING: this will be overridden if a Meta class is present in a subclass!
        # Make sure you include this line in your Meta class too.
        sequence = ("...", "actions")

        action_buttons = [ActionButtonType.EDIT, ActionButtonType.DELETE]
        """List of standard action buttons to render."""

        action_buttons_menu = []
        """Menu name for IActionButtons that should be rendered additionally to the standard buttons."""

        action_buttons_view_name = ""
        """View name for action buttons, like `<app_name>:<model_name>`.
        If not provided, it is auto-generated from the model."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._button_classes = []
        # get IActionButtons classes, filtered by menu name
        # and cache them, if there is an action_buttons_menu attribute
        self._action_buttons_menu = getattr(self.Meta, "action_buttons_menu", "")
        if self._action_buttons_menu:
            for menu_button_class in IActionButton.filter(self._action_buttons_menu):
                if menu_button_class not in self._button_classes:
                    self._button_classes.append(menu_button_class)
        self._action_buttons = getattr(
            self.Meta,
            "action_buttons",
            [ActionButtonType.EDIT, ActionButtonType.DELETE],
        )
        self._action_buttons_view_name = getattr(
            self.Meta,
            "action_buttons_view_name",
            f"{self.Meta.model._meta.app_label}:{self.Meta.model.__name__.lower()}",
        )

    def render_actions(self, record):
        """sort all desired buttons (standard and IActionMenu generated) by weight, and render them."""
        content_dict: dict[int, str] = {}
        for ActionButtonClass in self._button_classes:
            # instantiate action buttons with request and record
            item: IActionButton = ActionButtonClass(self.request, record)
            button = HxActionButton(self.request, item, dialog=True)
            weight = item.weight
            if weight in content_dict:
                content_dict[weight] += button.render(record)
            else:
                content_dict[weight] = button.render(record)
            del item
        # then render the standard action buttons like edit/delete
        for typ in self._action_buttons:
            button = ""
            match typ:
                case ActionButtonType.EDIT:
                    button = HxButton(
                        view_name=f"{self._action_buttons_view_name}:update",
                        icon="pencil",
                        dialog=True,
                        css_class="btn btn-action",
                        method="get",
                        title=_("Edit {object}").format(
                            object=self.Meta.model.__name__
                        ),
                    ).render(pk=record.id)
                case ActionButtonType.DELETE:
                    button = HxButton(
                        view_name=f"{self._action_buttons_view_name}:delete",
                        icon="trash",
                        dialog=True,
                        css_class="btn btn-action text-danger",
                        method="post",
                        title=_("Delete {object}").format(
                            object=self.Meta.model._meta.verbose_name
                        ),
                    ).render(pk=record.id)
                case ActionButtonType.DELETE_WITH_CONFIRM:
                    button = HxButton(
                        view_name=f"{self._action_buttons_view_name}:delete",
                        icon="trash",
                        dialog=True,
                        css_class="btn btn-action",
                        method="get",
                        title=_("Delete {object} without confirmation").format(
                            object=_(self.Meta.model._meta.verbose_name)
                        ),
                    ).render(pk=record.id)
            if typ in content_dict:
                content_dict[typ] += button
            else:
                content_dict[typ] = button

        ordered_dict = OrderedDict(sorted(content_dict.items()))
        return format_html("".join(ordered_dict.values()))


class HolidayPolicyTable(ActionButtonsTable):
    class Meta:
        model = HolidayPolicy
        fields = ("absence_type", "name")
        orderable = False
        sequence = ("...", "actions")
        action_buttons_menu = "holiday_policies_listitem"
        action_buttons = [ActionButtonType.EDIT, ActionButtonType.DELETE_WITH_CONFIRM]

    employees_list = tables.Column(verbose_name=_("Employees using this policy"))

    def render_employees_list(self, record: HolidayPolicy):
        count = len(record.employees.all())
        if not count:
            return ""
        string = ngettext("{count} employee", "{count} employees", count).format(
            count=count
        )
        return format_html(
            str(
                HxLink(
                    reverse(
                        "timetracker:holidaypolicy:employee-list", args=(record.pk,)
                    ),
                    string,
                    dialog=True,
                )
            )
        )


class PendingAbsenceRequestsTable(ActionButtonsTable):
    class Meta:
        model = Absence
        fields = ["employee", "day", "absence_type"]
        orderable = False
        sequence = ("...", "collisions", "actions")
        empty_text = _("No pending absence requests.")
        action_buttons_menu = "absence_requests_listitem"
        action_buttons = [ActionButtonType.DELETE]
        attrs = {"id": "pending-absence-requests-table"}

    collisions = tables.Column(verbose_name=_("Collisions"))

    def render_collisions(self, record: Absence) -> str:
        collisions = record.collisions()
        if not collisions:
            return ""
        return ", ".join([c.employee.name for c in collisions])


class AbsenceTypeTable(ActionButtonsTable):
    class Meta:
        model = AbsenceType
        fields = ["label", "color", "active", "needs_approval"]
        orderable = False
        sequence = ("...", "actions")


class MonthlyTimeBillingTable(tables.Table):
    class Meta:
        model = MonthlyTimeBilling
        fields = (
            "month",
            "created",
            "time_targeted",
            "time_worked",
            "time_delta",
            "payout_month",
            "time_balance_entry",
        )
        pagination = True

    created = tables.Column(verbose_name=_("Submitted"))
    time_delta = tables.Column(verbose_name=_("Time delta"))
    payout_month = tables.Column(verbose_name=_("Payout"))
    time_balance_entry = tables.Column(verbose_name=_("To time account"))

    def render_month(self, value: Month):
        link = reverse(
            "timetracker:timeentry:month-list",
            args=(value.year, value.month),
        )
        return format_html(f"<a href='{link}'>{month_name(value)} {value.year}</a>")

    def render_created(self, value: datetime):
        return value.date

    def render_time_targeted(self, value: timedelta):
        return as_hours(value)

    def render_time_worked(self, value: timedelta):
        return as_hours(value)

    def render_time_delta(self, value: timedelta):
        return prefix_plus(as_hours(value)) if value else ""

    def render_payout_month(self, value: Month):
        return format_html(f"{month_name(value)} {value.year}") if value else None

    def render_time_balance_entry(self, value: TimeBalanceEntry):
        return prefix_plus(as_hours(value.balance)) if value else ""

import calendar

import holidays
from calendar import (
    LocaleHTMLCalendar,
    different_locale,
)
from datetime import date
from dateutil.utils import today
from django.template import Engine, Context
from django.utils import timezone

from medux.common.models import Tenant
from medux.common.tools import country_code_from_locale
from medux.plugins.timetracker.models import (
    Absence,
    AbsenceStatus,
    TimetrackerEmployee,
)
from medux.preferences.definitions import Scope
from medux.preferences.models import ScopedPreference


def get_absence_context_for_day(
    employee: TimetrackerEmployee,
    thedate: date,
    absences: dict[int, Absence],
    locale: tuple[str, str] | str,
    themonth: int = None,
    country_holidays: dict[date, str] = None,
) -> dict:
    """Helper function that creates context for one holiday.

    Attributes:
        employee: the TimetrackerEmployee object
        thedate: the date of the day to render
        absences: a dict with absences for the day, grouped by employee_id:
        locale: the locale of the day to render, in the form ...TODO
        themonth: the month of the day to render. If thedate does not match themonth, an empty day is rendered.

    Returns:
        context: a dict with usable context for rendering one day:
            day: the date of the day to render
            object: the Holiday object of the employee
            type: the `HolidayType` of the holiday
            status: the `HolidayStatus` of the holiday
    """

    context = {}
    if not country_holidays:
        country_holidays = []
    context["day"] = thedate
    context["is_current_month"] = thedate.month == themonth if themonth else True
    employee_absence: Absence | None = None
    for employee_id, absence in absences.items():
        # find the current employee and create the context object from it
        if employee_id == employee.id:
            employee_absence = absence
            context["absence"] = absence
            context["absence_type"] = absence.absence_type
            context["status"] = absence.status
            context["approved"] = absence.status == AbsenceStatus.APPROVED
            context["declined"] = absence.status == AbsenceStatus.DECLINED
            context["new"] = absence.status == AbsenceStatus.NEW
            context["consumed"] = (
                absence.status == AbsenceStatus.APPROVED and thedate < today().date()
            )
            # remove the current employee from list, to use it as collisions list
            # del absences[index][employee_id]
    country = ScopedPreference.get_effective(
        "common", "country", Scope.TENANT, tenant=employee.tenant
    )
    is_working_day = employee.is_working_day(thedate, country=country)
    context["today"] = thedate == timezone.now().date()
    context["past"] = past = thedate < timezone.now().date()
    context["absences"] = absences
    is_public = thedate in country_holidays
    if is_public:
        context["public_holiday_name"] = country_holidays[thedate]
    context["public"] = is_public
    context["day_id"] = f"day-{thedate.year}-{thedate.month}-{thedate.day}"

    is_weekend = thedate.weekday() in [calendar.SATURDAY, calendar.SUNDAY]
    context["weekend"] = is_weekend

    context["editable"] = (
        not is_weekend
        and not is_public
        and (not employee_absence or employee_absence.status == AbsenceStatus.NEW)
        and is_working_day
        and (not past or employee.user.has_perm("timetracker.administer_absence"))
    )
    context["is_working_day"] = is_working_day
    return context


class HolidayCalendar(LocaleHTMLCalendar):
    cssclass_month_head = "border-light"
    cssclass_month = ""

    def __init__(self, employee: TimetrackerEmployee, locale=None, firstweekday=0):
        self.employee = employee
        # save available AbsenceTypes of current tenant for later use
        tenant = Tenant.objects.get(pk=employee.tenant.pk)
        self.absence_types = tenant.absence_types.all()
        self.locale = locale
        self.date_absences = {}
        super().__init__(firstweekday, locale)
        self.country = country_code_from_locale(locale)

    # noinspection PyMethodOverriding
    def formatday(self, thedate: date, themonth: int):
        """Render a day as a table cell."""
        absences = self.date_absences[thedate] if thedate in self.date_absences else {}
        context = get_absence_context_for_day(
            employee=self.employee,
            thedate=thedate,
            absences=absences,
            locale=self.locale,
            themonth=themonth,
            country_holidays=self.country_holidays,
        )
        context["absence_types"] = self.absence_types
        return (
            Engine.get_default()
            .get_template("timetracker/day.html")
            .render(Context(context))
        )

    # noinspection PyMethodOverriding
    def formatweek(self, theweek: list[date], themonth: int):
        s = "".join(self.formatday(thedate, themonth) for thedate in theweek)
        return f'<div class="d-flex d-xxl-inline-flex ">{s}</div>'

    def formatweekday(self, day: int) -> str:
        with different_locale(self.locale):
            return f'<div class="p-1 flex-fill {self.cssclasses_weekday_head[day]}">{calendar.day_abbr[day]}</div>'

    def formatweekheader(self):
        """
        Return a header for a week as a row.
        """
        s = "".join(self.formatweekday(i) for i in self.iterweekdays())
        return f"<div class='d-flex d-xxl-none'>{s}</div>"

    def formatmonth(self, theyear, themonth, withyear=True):
        v = []
        a = v.append
        a(
            '<div class="month d-inline-flex flex-column me-3 mb-3 d-xxl-flex flex-xxl-row me-xxl-0 mb-xxl-0">'
        )

        s = self.formatmonthname(theyear, themonth, withyear)
        a(
            f"<div class='h4 d-xxl-inline justify-content-end justify-content-xxl-start' style='width:6rem;'>{s}</div>"
        )

        a('<div class="header d-xxl-none">')
        a(f'<div class="">{self.formatweekheader()}</div>')
        a("</div>")

        a("<div class=''>")
        for week in self.monthdatescalendar(theyear, themonth):
            a(self.formatweek(week, themonth))
        a("</div>")

        a("</div>")
        return "".join(v)

    def formatyear(self, theyear, width=3):
        self.country_holidays = holidays.country_holidays(
            country=self.country, years=theyear
        )
        v = []
        a = v.append
        # use saved queryset to get all employees' absences from the database
        # and create a dict of absences, keyed by [day][employee_id]
        absences = Absence.objects.filter(
            employee__tenant=self.employee.tenant,
            day__year=theyear,
        )  # .annotate(num_absences=Count("id"))
        self.date_absences = {}
        for absence in absences:
            day = absence.day
            employee_id = absence.employee.id
            # num_absences = absence.num_absences
            if day not in self.date_absences:
                self.date_absences[day] = {}
            self.date_absences[day][employee_id] = absence

        # a(f"<div class='year year-{theyear}'")
        a('<div class="header d-xxl-none">')
        a(f'<div class="">{self.formatweekheader()}</div>')
        a("</div>")
        # a("</div>")
        for i, month in enumerate(range(1, 13)):
            a(self.formatmonth(theyear, month, withyear=False))
        return "".join(v)

from django.contrib.auth import get_user_model
from django.urls import path, include
from rest_framework import serializers, viewsets

from medux.plugins.timetracker import views
from medux.plugins.timetracker.models import TimeEntry
from medux.common.api.interfaces import IAdministrationURL

User = get_user_model()

app_name = "timetracker"


class TimeEntrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TimeEntry
        fields = "__all__"


class TimeEntryViewSet(viewsets.ModelViewSet):
    queryset = TimeEntry.objects.all()
    serializer_class = TimeEntrySerializer


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["url", "username", "email", "is_staff"]


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r"users", UserViewSet)
# router.register(r"timeentries", TimeEntryViewSet)


absence_urlpatterns = [
    path(
        "add/<str:absence_type>/",
        views.CreateAbsenceModalView.as_view(),
        name="add",
    ),
    path(
        "request/<int:year>/<int:month>/<int:day>/<str:absence_type>/",
        views.AbsenceRequestView.as_view(),
        name="request",
    ),
    path(
        "<pk>/approve/",
        views.AbsenceApproveView.as_view(),
        name="approve",
    ),
    path(
        "<pk>/decline/",
        views.AbsenceDeclineView.as_view(),
        name="decline",
    ),
    path(
        "<int:pk>/delete/",
        views.AbsenceDeleteView.as_view(),
        name="delete",
    ),
    path(
        "<pk>/cancel/",
        views.AbsenceCancelView.as_view(),
        name="cancel",
    ),
]

absence_type_urlpatterns = [
    path(
        "",
        views.AbsenceTypeListView.as_view(),
        name="list",
    ),
    path(
        "add/",
        views.AbsenceTypeCreateView.as_view(),
        name="add",
    ),
    path(
        "<int:pk>/change/",
        views.AbsenceTypeUpdateView.as_view(),
        name="update",
    ),
    path(
        "<int:pk>/delete/",
        views.AbsenceTypeDeleteView.as_view(),
        name="delete",
    ),
]

holidaypolicy_urlpatterns = [
    path(
        "",
        views.HolidayPolicyListView.as_view(),
        name="list",
    ),
    path(
        "add/",
        views.HolidayPolicyCreateView.as_view(),
        name="add",
    ),
    path(
        "update/<int:pk>/",
        views.HolidayPolicyUpdateView.as_view(),
        name="update",
    ),
    path(
        "delete/<int:pk>/",
        views.HolidayPolicyDeleteView.as_view(),
        name="delete",
    ),
    path(
        "<int:pk>/employees/",
        views.HolidayPolicyEmployeesView.as_view(),
        name="employee-list",
    ),
    path(
        "<int:pk>/employees/add/",
        views.HolidayPolicyEmployeeAddView.as_view(),
        name="employee-add",
    ),
    path(
        "hpa/<int:pk>/delete/",
        views.HolidayPolicyApplicationDeleteView.as_view(),
        name="employee-delete",
    ),
]

timeentry_urlpatterns = [
    path(
        "",
        views.TimeEntryMonthListView.as_view(),
        name="month-list",
    ),
    path("add/", views.TimeEntryCreateView.as_view(), name="add"),
    path("complete/", views.TimeEntryCompleteView.as_view(), name="complete"),
    path(
        "<pk>/change/",
        views.TimeEntryUpdateView.as_view(),
        name="update",
    ),
    path(
        "<pk>/delete/",
        views.TimeEntryDeleteView.as_view(),
        name="delete",
    ),
    path(
        "<int:year>/<int:month>/",
        views.TimeEntryMonthListView.as_view(),
        name="month-list",
    ),
]
urlpatterns = [
    # path("api/", include(router.urls)),
    path(
        "holidaypolicy/",
        include(
            (holidaypolicy_urlpatterns, "holidaypolicy"), namespace="holidaypolicy"
        ),
    ),
    path("absence/", include((absence_urlpatterns, "absence"), namespace="absence")),
    path(
        "absencetype/",
        include((absence_type_urlpatterns, "absencetype"), namespace="absencetype"),
    ),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("overview/", views.OverView.as_view(), name="overview"),
    path(
        "overview/<int:year>/<int:month>/",
        views.OverView.as_view(),
        name="overview-month",
    ),
    path("holidays/", views.HolidaysView.as_view(), name="current-holidays"),
    path("holidays/<int:year>/", views.HolidaysView.as_view(), name="holidays"),
    path(
        "pending-holidays/",
        views.PendingAbsenceRequestsView.as_view(),
        name="pending-absence-requests",
    ),
    path(
        "timeentry/",
        include((timeentry_urlpatterns, "timeentry"), namespace="timeentry"),
    ),
    path(
        "monthlytimebilling/",
        views.MonthlyTimeBillingListView.as_view(),
        name="monthlytimebilling-list",
    ),
    path(
        "monthlytimebilling/<int:year>/<int:month>/",
        views.MonthlyTimeBillingCreateView.as_view(),
        name="monthlytimebilling",
    ),
    path(
        "timebalanceentry/",
        views.TimeBalanceEntryListView.as_view(),
        name="time-balance-entry-list",
    ),
    # path(
    #     "api/check_payout_time_credit_hours",
    #     views.check_payout_time_credit_hours,
    #     name="check-check_payout_time_credit_hours",
    # ),
    path("archive", views.ArchiveView.as_view(), name="archive"),
    # WorkingTimeRange
    path(
        "work_schedule/",
        views.CurrentWorkScheduleView.as_view(),
        name="current-work-schedule",
    ),
    # TODO: should have no effect, place somewhere else?
    path(
        "absence/", views.AbsenceListWidgetView.as_view(), name="current-absence-days"
    ),
    # generic
    path("logout/", views.LogoutWithTimeTrackingView.as_view(), name="logout"),
]

admin_urlpatterns = []


class TimetrackerAdminURLs(IAdministrationURL):
    namespace = "timetracker"
    urlpatterns = [
        path(
            "pending-absence-requests/",
            views.admin.PendingAbsenceRequestsView.as_view(),
            name="pending-absence-requests",
        )
    ]

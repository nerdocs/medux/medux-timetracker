from django.contrib import admin

from medux.plugins.timetracker.forms import AbsenceForm
from medux.plugins.timetracker.models import (
    TimeEntry,
    Absence,
    AbsenceBalance,
    MonthlyTimeBilling,
    TimeBalanceEntry,
    AbsenceType,
    HolidayPolicy,
    HolidayPolicyApplication,
    TimetrackerEmployee,
)


@admin.register(TimeBalanceEntry)
class TimeBalanceEntryAdmin(admin.ModelAdmin):
    list_display = ["employee", "balance", "time_delta", "comment"]
    list_filter = ["employee"]
    ordering = ["created"]


@admin.register(TimeEntry)
class TimeEntryAdmin(admin.ModelAdmin):
    list_display = ["employee", "start", "end", "comment", "work_schedule"]
    list_filter = ["employee"]
    ordering = ["created"]


@admin.register(Absence)
class AbsenceAdmin(admin.ModelAdmin):
    list_display = ["employee", "day", "absence_type", "status"]
    list_display_links = ["day"]
    list_filter = ["employee", "absence_type", "status"]
    ordering = ["day"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "absence_type":
            current_absence_id = request.resolver_match.kwargs.get("object_id")
            if current_absence_id:
                current_absence = Absence.objects.get(pk=current_absence_id)
                kwargs["queryset"] = AbsenceType.objects.filter(
                    tenant=current_absence.employee.tenant
                )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class HolidayPolicyApplicationInline(admin.TabularInline):
    model = HolidayPolicyApplication
    extra = 0

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Limit the choices for the `employee` field to employees with the same
        tenant as the current holiday policy.
        """
        if db_field.name == "employee":
            # Get the employee object from the request object.
            holiday_policy_id = request.resolver_match.kwargs.get("object_id")
            try:
                holiday_policy = HolidayPolicy.objects.get(id=holiday_policy_id)
                # Limit the choices to groups with the same tenant as the employee.
                kwargs["queryset"] = TimetrackerEmployee.objects.filter(
                    tenant=holiday_policy.tenant
                )
            except HolidayPolicy.DoesNotExist:
                kwargs["queryset"] = HolidayPolicy.objects.none()

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(HolidayPolicy)
class HolidayPolicyAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "tenant",
        "employees_list",
        "days_per_year",
        "is_active",
        "is_standard",
        "is_absolute",
    ]
    inlines = [HolidayPolicyApplicationInline]


admin.site.register(MonthlyTimeBilling)


@admin.register(AbsenceType)
class AbsenceTypeAdmin(admin.ModelAdmin):
    list_filter = ["tenant"]
    list_display = [
        "label",
        "color",
        "needs_approval",
        "counts_as_negative_working_time",
        "tenant",
    ]


@admin.register(AbsenceBalance)
class AbsenceAbsenceBalanceAdmin(admin.ModelAdmin):
    list_filter = ["absence_type", "employee"]
    list_display = [
        "employee",
        "absence_type",
        "day",
        "balance",
        "comment",
    ]

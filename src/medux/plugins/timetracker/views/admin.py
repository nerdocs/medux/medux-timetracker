from django.urls import reverse
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django_tables2 import SingleTableView
from django.utils.translation import gettext_lazy as _

from medux.common.api.interfaces import ModalFormViewMixin
from medux.common.htmx.mixins import HtmxResponseMixin, HtmxDeleteView, HtmxFormMixin
from medux.common.views.mixins import TenantFilterMixin, PrepopulateFormViewMixin
from medux.common.views import AdministrationPageMixin, AdministrationTableListView
from medux.plugins.timetracker.forms import (
    HolidayPolicyForm,
    HolidayPolicyAddEmployeeForm,
    AbsenceTypeForm,
)
from medux.plugins.timetracker.models import (
    Absence,
    AbsenceStatus,
    HolidayPolicy,
    HolidayPolicyApplication,
    AbsenceType,
)
from medux.plugins.timetracker.tables import (
    HolidayPolicyTable,
    PendingAbsenceRequestsTable,
    AbsenceTypeTable,
)


class HolidayPolicyListView(
    TenantFilterMixin,
    AdministrationTableListView,
):
    model = HolidayPolicy
    table_class = HolidayPolicyTable
    permission_required = "timetracker.administer_absence"
    title = _("Holiday Policies")


class HolidayPolicyUpdateView(ModalFormViewMixin, PrepopulateFormViewMixin, UpdateView):
    model = HolidayPolicy
    form_class = HolidayPolicyForm
    permission_required = "timetracker.administer_absence"
    success_event = "timetracker:holidaypolicy:changed"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"tenant": self.request.employee.tenant})
        return kwargs

    def get_modal_title(self) -> str:
        return _("Update holiday policy:") + " " + self.object.name


class HolidayPolicyCreateView(ModalFormViewMixin, CreateView):
    model = HolidayPolicy
    form_class = HolidayPolicyForm
    permission_required = "timetracker.administer_absence"
    modal_title = _("Add holiday policy")
    success_event = "timetracker:holidaypolicy:changed"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"tenant": self.request.employee.tenant})
        return kwargs

    def form_valid(self, form):
        form.instance.tenant = self.request.user.tenant
        return super().form_valid(form)


class HolidayPolicyDeleteView(ModalFormViewMixin, DeleteView):
    model = HolidayPolicy
    template_name = "timetracker/adm_holidaypolicy_confirm_delete.html"
    permission_required = "timetracker.administer_absence"
    success_event = "timetracker:holidaypolicy:changed"

    def get_modal_title(self) -> str:
        return _("Delete holiday policy:") + " " + self.object.name

    def get_context_data(self, **kwargs):
        """Adds the number of employees that use this policy to the context"""
        context = super().get_context_data(**kwargs)
        context["applications"] = HolidayPolicyApplication.objects.filter(
            holiday_policy=self.object
        )
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.employees.count() == 0:
            return super().form_valid(self.get_form())
        return super().get(request, *args, **kwargs)


class HolidayPolicyEmployeesView(ModalFormViewMixin, ListView):
    template_name = "timetracker/adm_holidaypolicy_employees.html"
    model = HolidayPolicyApplication
    context_object_name = "applications"

    def get_queryset(self):
        return HolidayPolicyApplication.objects.filter(
            holiday_policy_id=self.kwargs.get("pk"),
            # for safety, add a filter to only show own tenant's policies
            employee__tenant_id=self.request.user.tenant,
        ).order_by("employee", "application_date")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["holiday_policy"] = HolidayPolicy.objects.get(pk=self.kwargs.get("pk"))
        return context


class HolidayPolicyEmployeeAddView(HtmxFormMixin, CreateView):
    model = HolidayPolicyApplication
    form_class = HolidayPolicyAddEmployeeForm
    success_event = "timetracker:holidaypolicy:changed"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["holiday_policy"] = HolidayPolicy.objects.get(pk=self.kwargs.get("pk"))
        return kwargs

    def get_success_url(self) -> str:
        return reverse(
            "timetracker:holidaypolicy:employee-add",
            kwargs={"pk": self.kwargs.get("pk")},
        )

    def get_context_data(self, **kwargs):
        """rename "form" to "add_form" so that the modal-form template can render it at a custom place."""
        context = super().get_context_data(**kwargs)
        context["add_form"] = context["form"]
        del context["form"]
        return context


class HolidayPolicyApplicationDeleteView(HtmxDeleteView):
    model = HolidayPolicyApplication
    success_event = "timetracker:holidaypolicy:deleted"


class PendingAbsenceRequestsView(AdministrationPageMixin, SingleTableView):
    """Shows all pending holiday requests from employees in a list."""

    model = Absence
    table_class = PendingAbsenceRequestsTable
    template_name = "timetracker/pending_holiday_list.html"
    permission_required = "timetracker.administer_absence"
    title = _("Pending absence requests")

    def get_queryset(self):
        return Absence.objects.filter(
            employee__tenant=self.request.user.tenant, status=AbsenceStatus.NEW
        ).order_by("day")


class AbsenceTypeListView(TenantFilterMixin, AdministrationTableListView):
    model = AbsenceType
    table_class = AbsenceTypeTable
    permission_required = "timetracker.administer_absence"
    title = _("Absence types")


class AbsenceTypeCreateView(ModalFormViewMixin, CreateView):
    model = AbsenceType
    form_class = AbsenceTypeForm
    permission_required = "timetracker.administer_absence"
    modal_title = _("Add absence type")
    success_event = "timetracker:absencetype:changed"

    def form_valid(self, form):
        form.instance.tenant = self.request.user.tenant
        return super().form_valid(form)


class AbsenceTypeUpdateView(ModalFormViewMixin, UpdateView):
    model = AbsenceType
    form_class = AbsenceTypeForm
    permission_required = "timetracker.administer_absence"
    modal_title = _("Edit absence type")
    success_event = "timetracker:absencetype:changed"

    def form_valid(self, form):
        form.instance.tenant = self.request.user.tenant
        return super().form_valid(form)


class AbsenceTypeDeleteView(HtmxDeleteView):
    model = AbsenceType
    permission_required = "timetracker.administer_absence"
    success_event = "timetracker:absencetype:changed"

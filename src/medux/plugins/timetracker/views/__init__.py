import locale
import calendar
import math
from datetime import timedelta, datetime, date

import holidays
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.db.models import Q

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views import View
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DateDetailView,
)
from django.utils.translation import gettext_lazy as _, to_locale, to_language
from django.views.generic.detail import (
    SingleObjectMixin,
)
from django_tables2 import SingleTableView
from holidays import WEEKEND
from month import Month

from medux.common.api.interfaces import ModalFormViewMixin
from medux.common.htmx.mixins import HtmxResponseMixin, HtmxDeleteView, HtmxFormMixin
from medux.common.models import Tenant
from medux.common.tools import monthdelta, round_up_time, country_code_from_locale
from medux.employees.models import WorkSchedule, WorkingContract
from medux.employees.views import EmployeeMixin
from medux.plugins.timetracker.constants import (
    ABSENCE_TYPE_ILLNESS,
    ABSENCE_TYPE_HOLIDAY,
)
from medux.plugins.timetracker.forms import (
    TimeEntryForm,
    LogoutWithTimeTrackingForm,
    AbsenceForm,
    MonthlyTimeBillingForm,
)
from medux.plugins.timetracker.models import (
    Absence,
    AbsenceStatus,
    TimeEntry,
    TimetrackerEmployee,
    MonthlyTimeBilling,
    TimeBalanceEntry,
)
from medux.plugins.timetracker import calendars
from .admin import (
    HolidayPolicyListView,
    HolidayPolicyUpdateView,
    HolidayPolicyCreateView,
    HolidayPolicyDeleteView,
    HolidayPolicyEmployeesView,
    HolidayPolicyEmployeeAddView,
    HolidayPolicyApplicationDeleteView,
    PendingAbsenceRequestsView,
    AbsenceTypeCreateView,
    AbsenceTypeListView,
    AbsenceTypeUpdateView,
    AbsenceTypeDeleteView,
)
from medux.core.models import User
from medux.common.api.http import HttpResponseEmpty, HttpResponseHXRedirect
from medux.common.templatetags.medux import as_hours
from medux.preferences.tools import get_effective_preference

__all__ = [
    "AbsenceTypesContextMixin",
    "CalendarContextMixin",
    "CreateAbsenceModalView",
    "HolidaysView",
    "OverView",
    "AbsenceApproveView",
    "AbsenceDeleteView",
    "AbsenceTypeListView",
    "AbsenceTypeUpdateView",
    "AbsenceTypeDeleteView",
    "TimeEntryDeleteView",
    "TimeEntryUpdateView",
    "TimeEntryDeleteView",
    "CurrentWorkScheduleView",
    "LogoutWithTimeTrackingView",
    "TimeEntryMonthListView",
    "MonthlyTimeBillingListView",
    "MonthlyTimeBillingCreateView",
    "TimeBalanceEntryListView",
    "CreateAbsenceModalView",
    "AbsenceListWidgetView",
    "ArchiveView",
    "AbsenceTypeCreateView",
    "HolidayPolicyListView",
    "HolidayPolicyUpdateView",
    "HolidayPolicyCreateView",
    "HolidayPolicyDeleteView",
    "HolidayPolicyEmployeesView",
    "HolidayPolicyEmployeeAddView",
    "HolidayPolicyApplicationDeleteView",
    "PendingAbsenceRequestsView",
    "AbsenceApproveView",
    "AbsenceDeclineView",
    "AbsenceRequestView",
]

from ..tables import MonthlyTimeBillingTable


class AbsenceTypesContextMixin:
    """Adds `absence_types` list (and `tenant`) to the context, to generate dynamic CSS colors."""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tenant = Tenant.objects.get(pk=self.request.user.tenant.pk)
        context["tenant"] = tenant
        context["absence_types"] = tenant.absence_types.all()
        return context


class CalendarContextMixin:
    """Adds calendar data to the context.

    Arguments:
        year: the year to create context data from. If not present, the current year is taken
        month: the month to create context data from. If not present, the current month is taken
    """

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        now = timezone.now()
        year = kwargs.get("year", now.year)
        month = kwargs.get("month", now.month)
        first_day = timezone.make_aware(datetime(year, month, 1))
        last_day = monthdelta(first_day, 1) - timedelta(days=1)
        prev_month = monthdelta(first_day, -1)
        next_month = monthdelta(first_day, 1)
        current_month = now.month
        current_year = now.year
        context.update(
            {
                "year": year,
                "month": month,
                "first_day": first_day,
                "last_day": last_day,
                "month_label": _(first_day.strftime("%B")),
                "prev_month": prev_month,
                "next_month": next_month,
                "prev_year": year - 1,
                "next_year": year + 1,
                "current_month": current_month,
                "current_year": current_year,
            }
        )
        return context


class HolidaysView(
    PermissionRequiredMixin,
    CalendarContextMixin,
    AbsenceTypesContextMixin,
    TemplateView,
):
    template_name = "timetracker/holidays.html"
    permission_required = "timetracker.view_absence"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # FIXME: add firstweekday as parameters
        # noinspection PyTypeChecker

        # try to get language from tenant preference
        pref = get_effective_preference("common", "language", self.request)
        if pref:
            language = pref.value
        else:
            # use settings default
            language, _, _ = to_language(settings.LANGUAGE_CODE).partition("-")

        pref = get_effective_preference("common", "country", self.request)
        if pref:
            country = pref.value
        else:
            # try to get country from default LANGUAGE_CODE
            _, _, country = to_locale(settings.LANGUAGE_CODE).partition("_")

        locale = f"{language}_{country}" if country else language
        cal = calendars.HolidayCalendar(
            employee=TimetrackerEmployee.objects.get(pk=self.request.employee.pk),
            firstweekday=calendar.MONDAY,
            locale=f"{locale}.UTF8",  # FIXME: allow others, e.g. iso8859-1
        )
        context["calendar"] = mark_safe(cal.formatyear(context["year"]))
        context["country_code"] = country
        return context


class OverView(PermissionRequiredMixin, CalendarContextMixin, TemplateView):
    template_name = "timetracker/overview.html"

    def has_permission(self):
        user = self.request.user
        return user.is_authenticated and user.has_perm("timetracker.view_timeentry")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        employee = self.request.employee
        if employee:
            employee.__class__ = TimetrackerEmployee

            # Generate a calendar
            now = timezone.now()
            month = context["month"] or now.month
            year = context["year"] or now.year

            calendar.Calendar(firstweekday=0).itermonthdates(year=year, month=month)
            # context["calendar"] = cal.monthdatescalendar(context["year"], context["month"])
            # context["form"] = AbsenceForm()
            context.update({"time_account_balance": employee.time_account_balance()})
        return context


class AbsenceListWidgetView(PermissionRequiredMixin, HtmxResponseMixin, ListView):
    """A widget that shows the list of absences that are configured for the employee.

    It shows the currently consumed absences, and the balance (s)he currently has."""

    template_name = "timetracker/absence_list_card.html"

    def has_permission(self):
        user = self.request.user
        return (
            user.has_perm("timetracker.view_absence")
            and self.request.employee is not None
        )

    def get_queryset(self):
        return Absence.objects.filter(
            employee=self.request.employee,
            # type__absencecontingent__employee=self.request.employee,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = timezone.now().date()
        employee: TimetrackerEmployee = TimetrackerEmployee.objects.get(
            pk=self.request.user.employee.pk
        )
        absences = Absence.objects.filter(
            employee=employee,
            day__lte=timezone.now().date(),
        )
        context["absence_types"] = []
        # get AbsenceTypes of current employee's tenant
        for absence_type in employee.tenant.absence_types.all():
            consumed = employee.absences_count(absence_type=absence_type)
            # len(absences.filter(type=absence_type,
            #                                                  status=AbsenceStatus.APPROVED))
            requested = len(
                absences.filter(absence_type=absence_type, status=AbsenceStatus.NEW)
            )
            balance = employee.absence_balance(
                reference_date=None, absence_type=absence_type
            )

            context["absence_types"].append(
                {
                    "type": absence_type,
                    "policy": employee.get_valid_holiday_policy(today, absence_type),
                    "count": employee.absences_count(absence_type=absence_type),
                    "consumed": consumed,
                    "requested": requested,
                    "percent": consumed / balance * 100 if balance else 0,
                    "balance": balance,
                }
            )
        context["from_date"] = employee.last_leave_year_start_before(
            today + timedelta(days=1)
        )
        context["to_date"] = employee.next_leave_year_start_after(
            today - timedelta(days=1)
        )

        return context


class AbsenceSetStatusView(
    HtmxFormMixin, PermissionRequiredMixin, SingleObjectMixin, View
):
    """Sets status (approved, declined,...) of the given Holiday.
    This class is not used directly, but inherited."""

    model = Absence
    fields = ["status"]
    status = AbsenceStatus.NEW
    success_event = "timetracker:absence:changed"

    def get_queryset(self):
        """Get a list of all absences of current user's tenant's employees"""
        # TODO: just fetch from Team, not tenant
        return Absence.objects.filter(
            employee__tenant=self.request.user.tenant, status=AbsenceStatus.NEW
        ).order_by("day")

    # def get_object(self, queryset=None):
    #     """Return"""
    #     if not queryset:
    #         queryset = self.get_queryset()
    #     try:
    #         return queryset.get(pk=self.kwargs.get("pk"))
    #     except self.model.DoesNotExist:
    #         return None

    def has_permission(self):
        user: User = self.request.user
        return (
            user.has_perm("timetracker.administer_absence")
            and self.get_object().employee.tenant == user.tenant
        )

    def post(self, request, **kwargs):
        self.object = self.get_object()
        self.object.status = self.status
        self.object.save()
        return HttpResponseEmpty(headers={"HX-Trigger": self.success_event})


class AbsenceApproveView(AbsenceSetStatusView):
    status = AbsenceStatus.APPROVED


class AbsenceDeclineView(AbsenceSetStatusView):
    status = AbsenceStatus.DECLINED


class AbsenceRequestView(
    HtmxFormMixin, AbsenceTypesContextMixin, PermissionRequiredMixin, DateDetailView
):
    """
    Creates a requested ("new") Absence object at specified day with given AbsenceType

    Attributes:
        year (int): year
        month (int): month
        day (int): day
        absence_type (AbsenceType): the AbsenceType UUID to create the requested Absence for

    Technically, this should be an CreateView. But there is no DateCreateView in Django, and I'm lazy.
    """

    template_name = "timetracker/day.html"
    model = Absence
    success_event = "timetracker:absence:changed"
    month_format = "%m"
    _date = None
    object: Absence | None = None

    def get_object(self, queryset=None):
        """Return"""
        if not queryset:
            queryset = self.get_queryset()
        try:
            return queryset.get(
                employee__id=self.request.employee.id, day=self.get_date()
            )
        except self.model.DoesNotExist:
            return None

    def has_permission(self):
        user: User = self.request.user
        if self.get_object():
            # always return False if there is already a requested absence
            return False
        return user.is_employee and user.has_perm("timetracker.add_absence")

    def get_date(self):
        if not self._date:
            self._date = timezone.make_aware(
                datetime(
                    self.kwargs.get("year"),
                    self.kwargs.get("month"),
                    self.kwargs.get("day"),
                )
            ).date()
        return self._date

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["absence"] = self.object
        the_date = self.get_date()
        absences = {
            a.employee.id: a
            for a in Absence.objects.filter(employee__tenant=self.request.user.tenant)
        }
        loc = locale.getlocale()
        country_code = country_code_from_locale(loc)
        country_holidays = holidays.country_holidays(country_code, years=the_date.year)
        context.update(
            calendars.get_absence_context_for_day(
                employee=TimetrackerEmployee.objects.get(pk=self.request.employee.pk),
                thedate=the_date,
                absences=absences,
                locale=loc,
                country_holidays=country_holidays,
            )
        )
        context["day"] = self.get_date()
        return context

    def post(self, request, **kwargs):
        # there is no absence marked on this day
        employee = self.request.user.employee
        tenant = Tenant.objects.get(pk=employee.tenant.pk)

        absence_type = tenant.absence_types.get(uuid=self.kwargs.get("absence_type"))
        # (
        # AbsenceType.objects.filter(
        #     Q(tenant=employee.tenant) | Q(tenant__isnull=True),
        #     uuid=self.kwargs.get("absence_type"),
        # )
        # .order_by("tenant")  # if there are 2: NULL and employee's tenant
        # .last()  # give the one WITH tenant priority
        # )
        # if absence type doesn't need to be approved, directly set APPROVED status.
        status = (
            AbsenceStatus.APPROVED
            if not absence_type.needs_approval
            else AbsenceStatus.NEW
        )
        self.object = Absence.objects.create(
            employee_id=employee.id,
            day=self.get_date(),
            absence_type=absence_type,
            status=status,
        )
        return self.render_to_response(self.get_context_data(**self.kwargs))


class AbsenceCancelView(
    PermissionRequiredMixin, AbsenceTypesContextMixin, HtmxDeleteView
):
    """Cancel an requested absence."""

    model = Absence
    template_name = "timetracker/day.html"
    day: date = None
    success_event = "timetracker:absence:changed"

    def has_permission(self):
        user: User = self.request.user
        self.object = self.get_object()
        return (
            user.is_employee
            and user.has_perm("timetracker.change_absence")
            and self.object.employee == user.employee
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["absence"] = self.object
        absences_for_day = {
            a.employee.id: a
            for a in Absence.objects.filter(
                employee__tenant=self.request.user.tenant, day=self.day
            )
        }

        context.update(
            calendars.get_absence_context_for_day(
                employee=TimetrackerEmployee.objects.get(pk=self.request.employee.pk),
                thedate=self.day,
                absences=absences_for_day,
                locale=locale.getdefaultlocale(),
            )
        )
        context["day"] = self.day
        return context

    def post(self, request, **kwargs):
        # self.object was already set by has_permission()
        self.day = self.object.day
        if (
            self.object.status == AbsenceStatus.NEW
            or self.object.absence_type.needs_approval is False
        ):
            # remove holiday request
            self.object.delete()
            self.object = None

        elif self.object.status == AbsenceStatus.APPROVED:
            # TODO: evtl. allow cancelling (STORNO)
            # don't change approved holidays
            messages.warning(
                self.request,
                _("Could not cancel absence, as it was already approved."),
            )

        return self.render_to_response(self.get_context_data(**self.kwargs))


class AbsenceDeleteView(PermissionRequiredMixin, HtmxDeleteView):
    model = Absence
    success_event = "timetracker:absence:deleted"

    def has_permission(self):
        user = self.request.user
        # only allow deleting own holidays, or if user has "administer" perm
        return user.has_perm("timetracker.delete_absence") and (
            user.employee.id == self.get_object().employee.id
            or user.has_perm("timetracker.administer_absence")
        )

    def get_queryset(self):
        return Absence.objects.filter(
            employee__tenant=self.request.user.tenant,
            status__in=[
                AbsenceStatus.APPROVED,
                AbsenceStatus.NEW,
                AbsenceStatus.DECLINED,
            ],
        ).order_by("day")


class TimeEntryCreateView(
    PermissionRequiredMixin, ModalFormViewMixin, HtmxFormMixin, CreateView
):
    """View to create time entries.

    it optionally takes a date, start_time and end_time GET parameters, to prefill the form.
    """

    model = TimeEntry
    form_class = TimeEntryForm
    permission_required = "timetracker.add_timeentry"
    modal_title = _("Add new time entry")
    success_event = "timetracker:timeentry:changed"

    def get_initial(self):
        initial = super().get_initial()
        date_param = self.request.GET.get("date")
        if date_param:
            initial["date"] = timezone.make_aware(
                datetime.strptime(date_param, "%Y-%m-%d")
            )
        elif self.request.method == "GET":
            raise AttributeError(
                f"{self.__class__.__name__} must be called with a 'date'"
                f"parameter if called in a GET request"
            )
        start_time = self.request.GET.get("start_time")
        end_time = self.request.GET.get("end_time")
        if start_time:
            initial["start_time"] = timezone.make_aware(
                datetime.strptime(start_time, "%H:%M").time()
            )

        if end_time:
            initial["end_time"] = timezone.make_aware(
                datetime.strptime(end_time, "%H:%M").time()
            )
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["employee"] = self.request.employee
        return kwargs


class TimeEntryCompleteView(TimeEntryCreateView):
    """Basically a CreateView, but fills out all open time slots of given day automatically, without asking the user.

    This view doesn't need a form really, just call it with a POST request.
    It needs a GET parameter `date` in the format YYYY-MM-DD, with the date of the time entries, which are then found
        automatically. This parameter is needed for POST requests too!
    """

    def post(self, request, **kwargs):
        date_param = self.request.GET.get("date")
        day = timezone.make_aware(datetime.strptime(date_param, "%Y-%m-%d")).date()
        employee = self.request.user.employee
        employee.__class__ = TimetrackerEmployee
        for wtr in employee.working_time_ranges_for_day(day):  # type:WorkingTimeRange
            TimeEntry.objects.create(
                start=datetime.combine(day, wtr.start_time),
                end=datetime.combine(day, wtr.end_time),
                employee=employee,
            )
        return HttpResponseEmpty()


class TimeEntryUpdateView(ModalFormViewMixin, PermissionRequiredMixin, UpdateView):
    model = TimeEntry
    form_class = TimeEntryForm
    permission_required = "timetracker.change_timeentry"
    modal_title = _("Edit time entry")
    success_event = "timetracker:timeentry:changed"

    def get_initial(self):
        initial = super().get_initial()
        instance = self.get_object()
        initial["date"] = timezone.localdate(instance.start)
        initial["start_time"] = timezone.localtime(instance.start).time()
        # if there is an end time, put it into "end_time".
        # if not, don't take current time.
        if instance.end:
            initial["end_time"] = timezone.localtime(instance.end).time()
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"employee": self.request.employee})
        return kwargs


class LogoutWithTimeTrackingView(TimeEntryUpdateView):
    """A modal form view that allows finishing a time entry before logging out."""

    template_name = "timetracker/logout_with_timetracking_form.html"
    modal_title = _("Logout with time tracking")
    model = TimeEntry

    def get_object(self, queryset=None):
        employee = TimetrackerEmployee.objects.get(id=self.request.employee.id)
        now = timezone.now()
        try:
            # first, try to get open time entry from today
            return self.model.objects.get(
                employee=self.request.employee,
                start__date=timezone.now().date(),
                end__isnull=True,
            )
        except TimeEntry.DoesNotExist:
            # if no open time entry exists, create new one with today's data.

            # first, get WorkSchedule for today, None if not applicable (Sunday etc.)
            work_schedule: WorkSchedule = employee.active_work_schedule(now.date())
            # TODO: this is ReallyBad™ - ideally try to get WS that matches current time.
            # but for most cases it will do, as there mostly are not 2 WS/day
            weekday = now.weekday()
            # get WTR that matches current time (+ 1h)
            wtrs = work_schedule.working_hours.filter(
                weekday=weekday,
                start_time__lt=now,
                end_time__gte=now + timedelta(hours=1),
            )
            # if no WTR defined for today's weekday, make start=now()
            start_time = wtrs.first().start_time if wtrs.exists() else now

            return TimeEntry(
                employee=employee,
                work_schedule=work_schedule,
                start=datetime.combine(now.date(), start_time.time()),
            )

    def get_form(self, form_class=None):
        employee = self.request.employee
        initial = {
            "date": timezone.localdate(),
            "end_time": round_up_time(timezone.localtime(), 15),
        }
        try:
            # try to fetch open TimeEntry from today
            instance = TimeEntry.objects.get(
                employee=employee, start__date=timezone.localdate(), end=None
            )
            initial["start_time"] = timezone.localtime(instance.start)
        except TimeEntry.DoesNotExist:
            instance = None

        form = LogoutWithTimeTrackingForm(
            data=self.request.POST or None,
            instance=instance,
            employee=employee,
            initial=initial,
        )
        return form

    def form_valid(self, form):
        # save the time entry
        super().form_valid(form)
        # and log user out.
        logout(self.request)
        return HttpResponseHXRedirect(redirect_to=settings.LOGIN_URL)


class TimeEntryDeleteView(PermissionRequiredMixin, HtmxDeleteView):
    model = TimeEntry
    success_event = "timetracker:timeentry:changed"

    def has_permission(self):
        user = self.request.user
        return (
            user.is_employee
            and user.has_perm("timetracker.delete_timeentry")
            and self.get_object().employee == user.employee
        )


class TimeEntryMonthListView(
    PermissionRequiredMixin, AbsenceTypesContextMixin, ListView
):
    model = TimeEntry
    template_name = "timetracker/timeentry_month_list.html"

    def has_permission(self):
        user = self.request.user
        return user.is_authenticated and user.is_employee

    def get(self, request, *args, **kwargs):
        now = timezone.now()
        self.month = Month(
            year=self.kwargs.get("year", now.year),
            month=self.kwargs.get("month", now.month),
        )

        self.next_month = self.month + 1
        self.prev_month = self.month - 1
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        employee = self.request.employee
        if not employee:
            messages.error(
                self.request,
                _("Time entries are only available for employees."),
            )
            return TimeEntry.objects.none()
        employee.__class__ = TimetrackerEmployee

        # if not WorkSchedule.objects.filter(employee=employee).exists():
        #     messages.info(
        #         self.request,
        #         _("Please let your employer define some working time ranges first."),
        #         extra_tags="dismissible",
        #     )
        return employee.monthly_time_entries(self.month).order_by("start")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.request.employee:
            return context

        employee: TimetrackerEmployee = TimetrackerEmployee.objects.get(
            pk=self.request.employee.id
        )
        day_entries = []
        first_day_of_month = self.month.first_day()

        # only allow submitting month if we are in next month
        context["allow_submitting"] = (
            timezone.localdate(timezone.now()) >= (self.month + 1).first_day()
        )

        local_holidays = holidays.country_holidays(
            get_effective_preference("common", "country", self.request).value
        )
        # get employee's absences and put them in to a date keyed dict
        employee_absences = Absence.objects.filter(
            employee=employee, day__month=self.month.month, day__year=self.month.year
        )
        absences: dict[date, Absence] = {}
        for a in employee_absences:
            absences[a.day] = a
        del employee_absences

        all_absences_approved = True
        # check if working contract starts before and ends after this month (which is the most common case),
        # and if so, cache it for later.
        month_contract = WorkingContract.objects.filter(
            Q(start_date__lte=self.month.first_day()),
            Q(end_date__isnull=True) | Q(end_date__gte=self.month.last_day()),
        ).first()

        time_entries = self.get_queryset().all()
        month_target_working_time = timedelta()  # planned working time this month
        month_time_entry_sum = timedelta()
        for day in calendar.Calendar().itermonthdays3(
            self.month.year, self.month.month
        ):
            current_date = date(*day)
            if current_date.month != self.month.month:
                continue

            # first check if there is an active working contract for current day
            if month_contract:
                # we have a cached contract for whole month.
                active_contract = month_contract
            else:
                active_contract: WorkingContract = employee.active_working_contract(
                    current_date
                )
            if not active_contract:
                # no active working contract for current day
                # messages.info(
                #     self.request,
                #     _(
                #         "There is no working contract defined. "
                #         "Please ask your employee to create one."
                #     ),
                # )
                continue

            # initialize entry
            day_entry: dict = {
                "date": current_date,
                "time_entries": [],
                # "working_time_ranges": working_time_ranges,
            }

            # check if any time_entry matches current date
            for time_entry in time_entries:
                # if start time of the entry is before current_date
                if timezone.localdate(time_entry.start) == current_date:
                    day_entry["time_entries"].append(time_entry)

            # check if day is a holiday
            is_public_holiday = current_date in local_holidays
            if is_public_holiday:
                day_entry["is_public_holiday"] = is_public_holiday
                day_entry["holiday_name"] = local_holidays[current_date]

            if current_date in absences:
                absence = absences[current_date]
                day_entry["absence"] = absence
                # day_entry[
                #     "counts_negative_time"
                # ] = absence.absence_type.counts_as_negative_working_time
                match absence.status:
                    case AbsenceStatus.NEW:
                        day_entry["absence_status_requested"] = True
                        all_absences_approved = False
                    case AbsenceStatus.APPROVED:
                        day_entry["absence_status_approved"] = True
                    case AbsenceStatus.DECLINED:
                        day_entry["absence_status_declined"] = True
            else:
                absence = None

            daily_time_entry_sum = employee.daily_time_entry_sum(current_date)
            month_time_entry_sum += daily_time_entry_sum

            day_entry["is_weekend"] = current_date.weekday() in WEEKEND
            day_entry["today"] = current_date == timezone.now().date()

            # filter out all kinds of holidays, illness etc.: don't count them for
            # target time!
            is_working_day = not is_public_holiday and (
                not absence
                or absence.status != AbsenceStatus.APPROVED
                or absence.absence_type.counts_as_negative_working_time
            )
            # if day is a public holiday, absence, illness etc., don't count it as
            # working day
            day_entry["is_working_day"] = is_working_day

            # don't count working time ranges on non-working-days
            working_time_ranges = (
                employee.working_time_ranges_for_day(current_date)
                if is_working_day
                else []
            )
            day_entry["working_time_ranges"] = working_time_ranges
            day_entry["active_work_schedule"] = employee.active_work_schedule(
                current_date
            )
            if working_time_ranges:
                weekday_matches = current_date.weekday() in [
                    wtr.weekday for wtr in working_time_ranges
                ]
            else:
                weekday_matches = False

            # only count daily normal working time if
            # * no absence present
            # * absence is approved and absence is marked for counting.
            daily_normal_working_time = (
                employee.daily_normal_working_time(current_date, working_time_ranges)
                if is_working_day
                else timedelta()
            )
            day_entry["is_missing"] = (
                current_date < timezone.now().date()
                and not day_entry["time_entries"]
                and not is_public_holiday
                and weekday_matches
            )
            day_entry["actions_enabled"] = (
                absence and not absence.status == AbsenceStatus.DECLINED
            ) or daily_normal_working_time

            # if there are no time entries on this day, create a dummy one.
            if not day_entry["time_entries"]:
                day_entry["time_entries"].append(None)

            day_entries.append(day_entry)

            day_entry["daily_normal_working_time"] = (
                daily_normal_working_time.total_seconds() / 60 / 60
            )
            day_entry["daily_timedelta"] = (
                (daily_time_entry_sum - daily_normal_working_time).total_seconds()
                / 60
                / 60
            )
            month_target_working_time += daily_normal_working_time

        context.update(
            {
                "month": self.month,  # FIXME
                "next_month": self.next_month,
                "prev_month": self.prev_month,
                "today": timezone.now().date(),
                "day_entries": day_entries,
                "weekend": WEEKEND,
                "all_absences_approved": all_absences_approved,
                "month_target_working_time": month_target_working_time,
                "month_current_working_time": month_time_entry_sum,
                "month_working_time_delta": month_time_entry_sum
                - month_target_working_time,
                "month_current_working_time_percent": math.ceil(
                    month_time_entry_sum / month_target_working_time * 100
                )
                if month_target_working_time
                else 0,
                "submitted": MonthlyTimeBilling.objects.filter(
                    employee=employee, month=first_day_of_month
                ).exists(),
                "time_account_balance": as_hours(
                    TimeBalanceEntry.objects.filter(employee=self.request.employee)
                    .last()
                    .balance
                ),
            }
        )
        return context


# def check_payout_time_credit_hours(request):
#     """Small helper view that checks the  for correctness"""
#     # TODO: check permissions!
#     if not request.htmx:
#         raise PermissionDenied("Method is only allowed over HTMX.")
#     form = MonthlyTimeBillingForm(data=request.GET)
#     return HttpResponse(as_crispy_field(form["payout_time_credit_hours"]))


class TimeBalanceEntryListView(
    PermissionRequiredMixin, HtmxResponseMixin, EmployeeMixin, ListView
):
    model = TimeBalanceEntry
    permission_required = "timetracker.view_timeentry"
    template_name = "timetracker/timebalanceentry_list_card.html"


class MonthlyTimeBillingListView(
    PermissionRequiredMixin, EmployeeMixin, SingleTableView  # EmployeeMixin,
):
    model = MonthlyTimeBilling
    table_class = MonthlyTimeBillingTable
    permission_required = "timetracker.view_timeentry"
    template_name = "timetracker/monthlytimebilling_list_card.html"

    def get_queryset(self):
        """additionally fetch attached time_balance_entry from MonthlyTimeBilling"""
        return super().get_queryset().select_related("time_balance_entry")


class MonthlyTimeBillingCreateView(
    PermissionRequiredMixin, ModalFormViewMixin, UpdateView
):
    """ModalView for employees to submit a time billing for one month."""

    model = MonthlyTimeBilling
    form_class = MonthlyTimeBillingForm
    permission_required = "timetracker.add_timeentry"
    template_name = "timetracker/month_submission_form.html"
    success_event = "timetracker:monthlytimebilling:created"

    def get_object(self, queryset=None):
        employee = self.request.employee
        # Don't look at the following code line, it's bad for your eyes.
        employee.__class__ = TimetrackerEmployee
        self.month = Month(self.kwargs.get("year"), self.kwargs.get("month"))

        obj = MonthlyTimeBilling(employee=employee, month=self.month)
        obj.recalculate()
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        employee = self.request.employee
        employee.__class__ = TimetrackerEmployee
        context.update(
            {
                "month_name": _(calendar.month_name[self.month.month]),
                "year": self.month.year,
                "time_delta": self.object.time_delta.total_seconds() / 3600,
                "time_account_balance": as_hours(employee.time_account_balance()),
            }
        )
        return context

    def form_valid(self, form):
        instance: MonthlyTimeBilling = form.save(commit=False)
        employee = self.request.employee
        employee.__class__ = TimetrackerEmployee
        instance.employee = employee
        instance.recalculate()
        instance.save()

        # calculate remaining time for adding to time account
        remaining_time_for_time_account = (
            instance.time_delta  # +/- this month
            - timedelta(seconds=int(instance.payout_time_credit_hours * 3600))
        )
        TimeBalanceEntry.objects.create(
            employee=employee,
            time_delta=remaining_time_for_time_account,
            time_billing=instance,
        )
        return HttpResponseEmpty(headers={"HX-Trigger": self.success_event})


class CurrentWorkScheduleView(HtmxResponseMixin, PermissionRequiredMixin, DetailView):
    model = WorkSchedule
    template_name = "timetracker/work_schedule_detail_card.html"

    def has_permission(self):
        return (
            self.request.user.has_perm("employees.view_workschedule")
            and self.request.employee is not None
        )

    def get_queryset(self):
        return self.model.objects.filter(employee=self.request.employee)

    def get_object(self, queryset=None):
        if not self.kwargs.get("pk", None):
            return WorkSchedule.get_active(self.request.employee)
        return super().get_object(queryset)


class CreateAbsenceModalView(PermissionRequiredMixin, ModalFormViewMixin, CreateView):
    model = Absence
    permission_required = "timetracker.view_absence"
    form_class = AbsenceForm
    success_event = "timetracker:absence:changed"

    def get_modal_title(self) -> str:
        at = self.kwargs["absence_type"]
        if at == ABSENCE_TYPE_HOLIDAY:
            return _("Request holidays")
        elif at == ABSENCE_TYPE_ILLNESS:
            return _("Add sick leave")
        else:
            raise AttributeError(
                f"Absence type {self.kwargs['absence_type']} not supported."
            )

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        kwargs["absence_type"] = self.kwargs["absence_type"]
        return kwargs


class ArchiveView(PermissionRequiredMixin, TemplateView):
    permission_required = "timetracker.view_timeentry"
    template_name = "timetracker/archive.html"

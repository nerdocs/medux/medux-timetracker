from datetime import timedelta

from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

from medux.common.models import Tenant
from medux.employees.models import (
    Employee,
    WorkSchedule as EmployeeWorkSchedule,
    WorkingContract,
    WorkingTimeRange,
)
from medux.plugins.timetracker.constants import ABSENCE_TYPE_PRESETS
from medux.plugins.timetracker.models import (
    TimeEntry,
    TimeBalanceEntry,
    TimetrackerEmployee,
    WorkSchedule,
    AbsenceBalance,
    AbsenceType,
)

User = get_user_model()


def notify_employee_after_login_for_open_timeentries(sender, **kwargs):
    user = kwargs["user"]
    if hasattr(user, "employee"):
        # search for started, but unfinished timeentries of the current user
        TimeEntry.objects.filter(
            employee=user.employee,
            end=None,
            start__lt=now() + timedelta(minutes=15),
            # work_schedule__working_hours__week_day=today().weekday(),
        )
        # there are unfinished timeentries, notify user
        # messages.info(
        #     sender.request,
        #     "There are unfinished time entries.",  # TODO link to Timetracker
        #     extra_tags="dismissible",
        # )


user_logged_in.connect(notify_employee_after_login_for_open_timeentries, sender=User)


@receiver(post_save, sender=WorkSchedule, dispatch_uid="employee_work_schedule_saved")
@receiver(
    post_save, sender=EmployeeWorkSchedule, dispatch_uid="employee_work_schedule_saved"
)
def bind_matching_time_entries_to_working_schedule_after_saving(
    sender, instance: WorkSchedule, **kwargs
):
    """After saving a WorkSchedule (with possibly new start/end dates),
    find all TimeEntries that match its date range, and bind them using
    their .working_hours FK.
    """
    # first, set all "old" time entries free
    instance.time_entries.all().update(work_schedule=None)
    # then find TimeEntries of matching employee and after new WS start date
    fltr = {"employee": instance.employee, "start__gte": instance.start_date}
    # if WS has no end date, take 'em all
    if instance.end_date:
        # if there is an end date, only get TimeEntries before it
        fltr["end__lte"] = instance.end_date
    TimeEntry.objects.filter(**fltr).update(work_schedule=instance)


@receiver(post_save, sender=Employee, dispatch_uid="employee_saved_create_timebalance")
@receiver(
    post_save,
    sender=TimetrackerEmployee,
    dispatch_uid="employee_saved_create_timebalance",
)
def create_time_balance_for_created_employee(sender, instance, created: bool, **kwargs):
    """Create an empty time balance for each newly created Employee"""
    if issubclass(sender, Employee) and created:
        TimeBalanceEntry.objects.create(
            employee=instance, balance=timedelta(0), comment=_("Initial entry")
        )


# def create_holiday_entitlements_after_work_schedule_save(
#     sender, instance: WorkSchedule, created: bool, **kwargs
# ):
#     if issubclass(sender, WorkSchedule) and created:
#         instance.create_holiday_entitlements()
#
#
# post_save.connect(
#     create_holiday_entitlements_after_work_schedule_save, sender=WorkSchedule
# )


def create_absence_types_for_tenant(sender, instance: Tenant, created: bool, **kwargs):
    """Initialize the Timetracker plugin."""
    from .models import AbsenceType

    if issubclass(sender, Tenant) and created:
        # create AbsenceType presets
        print(" i Creating AbsenceType presets for {tenant}...".format(tenant=instance))
        for uuid in ABSENCE_TYPE_PRESETS:
            try:
                AbsenceType.objects.get(uuid=uuid)
            except AbsenceType.DoesNotExist:
                AbsenceType.objects.create(
                    **ABSENCE_TYPE_PRESETS[uuid], tenant=instance
                )


post_save.connect(create_absence_types_for_tenant, sender=Tenant)


@receiver(post_save, sender=WorkingContract, dispatch_uid="working_contract_saved")
def working_contract_saved(sender, instance: WorkingContract, created, **kwargs):
    """After saving a contract, make sure an absence balance entry for each AbsenceType
    exists at contract start."""
    for absence_type in AbsenceType.objects.filter(tenant=instance.tenant):
        AbsenceBalance.objects.get_or_create(
            employee=instance.employee,
            absence_type=absence_type,
            day=instance.start_date,
            balance=0,
            comment=_("Initial entry"),
        )


@receiver(post_save, sender=WorkingTimeRange, dispatch_uid="wtr_saved")
def working_time_range_saved_recalculate_absences(
    sender, instance: WorkingTimeRange, created, **kwargs
):
    if created:
        instance.work_schedule.recalculate_absence_entitlements()
